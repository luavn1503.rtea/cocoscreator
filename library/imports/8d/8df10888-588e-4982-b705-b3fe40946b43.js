"use strict";
cc._RF.push(module, '8df10iIWI5JgrcFs/5AlGtD', 'InGameUI');
// scripts/UI/InGameUI.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {
    waveUI: cc.Node,
    killDisplay: cc.Node,
    comboDisplay: cc.Node,
    pauseUI: cc.Node //waveProgress: cc.Node

  },
  // use this for initialization
  init: function init(game) {
    this.waveUI = this.waveUI.getComponent('WaveUI');
    this.waveUI.node.active = false;
    this.killDisplay = this.killDisplay.getComponent('KillDisplay');
    this.killDisplay.node.active = false;
    this.comboDisplay = this.comboDisplay.getComponent('ComboDisplay');
    this.comboDisplay.init(); // this.waveProgress = this.waveProgress.getComponent('WaveProgress');
    // this.waveProgress.init(game.waveMng);
  },
  showWave: function showWave(num) {
    this.waveUI.node.active = true;
    this.waveUI.show(num);
  },
  showKills: function showKills(num) {
    this.killDisplay.playKill(num);
  },
  addCombo: function addCombo() {
    this.comboDisplay.playCombo();
  },
  showPopupPauseUI: function showPopupPauseUI() {
    this.pauseUI.node.active = true;
  }
});

cc._RF.pop();