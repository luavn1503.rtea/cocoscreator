"use strict";
cc._RF.push(module, 'bd907OrsYVGJpMAP56xffaF', 'Projectile');
// scripts/Actors/Projectile.js

"use strict";

var ProjectileType = require('Types').ProjectileType;

cc.Class({
  "extends": cc.Component,
  properties: {
    projectileType: {
      "default": ProjectileType.Arrow,
      type: ProjectileType
    },
    sprite: cc.Sprite,
    fxBroken: cc.Animation,
    moveSpeed: 0,
    canBreak: true
  },
  // use this for initialization
  init: function init(waveMng, dir) {
    this.waveMng = waveMng;
    this.player = waveMng.player;
    var rad = Math.atan2(dir.y, dir.x);
    var deg = cc.misc.radiansToDegrees(rad);
    var rotation = 90 - deg;
    this.sprite.node.rotation = rotation;
    this.sprite.enabled = true;
    this.direction = dir.normalize();
    this.isMoving = true;
  },
  broke: function broke() {
    this.isMoving = false;
    this.sprite.enabled = false;
    this.fxBroken.node.active = true;
    this.fxBroken.play('arrow-break');
  },
  hit: function hit() {
    this.isMoving = false;
    this.onBrokenFXFinished();
  },
  onBrokenFXFinished: function onBrokenFXFinished() {
    this.fxBroken.node.active = false;
    this.waveMng.despawnProjectile(this);
  },
  update: function update(dt) {
    if (this.isMoving === false) {
      return;
    }

    var dist = this.player.node.position.sub(this.node.position).mag();

    if (dist < this.player.hurtRadius && this.player.isAlive) {
      if (this.canBreak && this.player.isAttacking) {
        this.broke();
        return;
      } else {
        this.player.dead();
        this.hit();
        return;
      }
    }

    if (this.isMoving) {
      this.node.x += this.moveSpeed * this.direction.x * dt;
      this.node.y += this.moveSpeed * this.direction.y * dt;

      if (Math.abs(this.node.x) > this.waveMng.foeGroup.width / 2 || Math.abs(this.node.y) > this.waveMng.foeGroup.height / 2) {
        this.onBrokenFXFinished();
      }
    }
  }
});

cc._RF.pop();