"use strict";
cc._RF.push(module, 'd887bx46FZAa4lxVyJ37BSp', 'Foe');
// scripts/Actors/Foe.js

"use strict";

var MoveState = require('Move').MoveState;

var FoeType = require('Types').FoeType;

var ProjectileType = require('Types').ProjectileType;

var AttackType = cc.Enum({
  Melee: -1,
  Range: -1
});
cc.Class({
  "extends": cc.Component,
  properties: {
    foeType: {
      "default": FoeType.Foe0,
      type: FoeType
    },
    atkType: {
      "default": AttackType.Melee,
      type: AttackType
    },
    projectileType: {
      "default": ProjectileType.Arrow,
      type: ProjectileType
    },
    hitPoint: 0,
    hurtRadius: 0,
    atkRange: 0,
    atkDist: 0,
    atkDuration: 0,
    atkStun: 0,
    atkPrepTime: 0,
    corpseDuration: 0,
    sfAtkDirs: [cc.SpriteFrame],
    fxSmoke: cc.ParticleSystem,
    fxBlood: cc.Animation,
    fxBlade: cc.Animation
  },
  init: function init(waveMng) {
    this.waveMng = waveMng;
    this.player = waveMng.player;
    this.isAttacking = false;
    this.isAlive = false;
    this.isInvincible = false;
    this.isMoving = false;
    this.hp = this.hitPoint;
    this.move = this.getComponent('Move');
    this.anim = this.move.anim;
    this.spFoe = this.anim.getComponent(cc.Sprite);
    this.bloodDuration = this.fxBlood.getAnimationState('blood').clip.duration;
    this.fxBlood.node.active = false;
    this.fxBlade.node.active = false;

    if (this.anim.getAnimationState('born')) {
      this.anim.play('born');
    } else {
      this.readyToMove();
    }
  },
  update: function update(dt) {
    if (this.isAlive === false) {
      return;
    }

    var dist = this.player.node.position.sub(this.node.position).mag();

    if (this.player.isAttacking && this.isInvincible === false) {
      if (dist < this.hurtRadius) {
        this.dead();
        return;
      }
    }

    if (this.isAttacking && this.player.isAlive) {
      if (dist < this.player.hurtRadius) {
        this.player.dead();
        return;
      }
    }

    if (this.player && this.isMoving) {
      var dir = this.player.node.position.sub(this.node.position);
      var rad = Math.atan2(dir.y, dir.x);
      var deg = cc.misc.radiansToDegrees(rad);

      if (dist < this.atkRange) {
        this.prepAttack(dir);
        return;
      }

      this.node.emit('update-dir', {
        dir: dir.normalize()
      });
    }
  },
  readyToMove: function readyToMove() {
    this.isAlive = true;
    this.isMoving = true;
    this.fxSmoke.resetSystem();
  },
  prepAttack: function prepAttack(dir) {
    var animName = '';

    if (Math.abs(dir.x) >= Math.abs(dir.y)) {
      animName = 'pre_atk_right';
    } else {
      if (dir.y > 0) {
        animName = 'pre_atk_up';
      } else {
        animName = 'pre_atk_down';
      }
    }

    this.node.emit('freeze');
    this.anim.play(animName);
    this.isMoving = false;
    this.scheduleOnce(this.attack, this.atkPrepTime);
  },
  attack: function attack() {
    if (this.isAlive === false) {
      return;
    }

    this.anim.stop();
    var atkDir = this.player.node.position.sub(this.node.position);
    var targetPos = null;

    if (this.atkType === AttackType.Melee) {
      targetPos = this.node.position.add(atkDir.normalize().mul(this.atkDist));
    }

    this.attackOnTarget(atkDir, targetPos);
  },
  attackOnTarget: function attackOnTarget(atkDir, targetPos) {
    var deg = cc.misc.radiansToDegrees(cc.v2(0, 1).signAngle(atkDir));
    var angleDivider = [0, 45, 135, 180];
    var slashPos = null;

    function getAtkSF(mag, sfAtkDirs) {
      var atkSF = null;

      for (var i = 1; i < angleDivider.length; ++i) {
        var min = angleDivider[i - 1];
        var max = angleDivider[i];

        if (mag <= max && mag > min) {
          atkSF = sfAtkDirs[i - 1];
          return atkSF;
        }
      }

      if (atkSF === null) {
        cc.error('cannot find correct attack pose sprite frame! mag: ' + mag);
        return null;
      }
    }

    var mag = Math.abs(deg);

    if (deg <= 0) {
      this.anim.node.scaleX = 1;
      this.spFoe.spriteFrame = getAtkSF(mag, this.sfAtkDirs);
    } else {
      this.anim.node.scaleX = -1;
      this.spFoe.spriteFrame = getAtkSF(mag, this.sfAtkDirs);
    }

    var delay = cc.delayTime(this.atkStun);
    var callback = cc.callFunc(this.onAtkFinished, this);

    if (this.atkType === AttackType.Melee) {
      var moveAction = cc.moveTo(this.atkDuration, targetPos).easing(cc.easeQuinticActionOut());
      this.node.runAction(cc.sequence(moveAction, delay, callback));
      this.isAttacking = true;
    } else {
      if (this.projectileType === ProjectileType.None) {
        return;
      }

      this.waveMng.spawnProjectile(this.projectileType, this.node.position, atkDir);
      this.node.runAction(cc.sequence(delay, callback));
    }
  },
  onAtkFinished: function onAtkFinished() {
    this.isAttacking = false;

    if (this.isAlive) {
      this.isMoving = true;
    }
  },
  dead: function dead() {
    this.move.stop();
    this.isMoving = false;
    this.isAttacking = false;
    this.anim.play('dead');
    this.fxBlood.node.active = true;
    this.fxBlood.node.scaleX = this.anim.node.scaleX;
    this.fxBlood.play('blood');
    this.fxBlade.node.active = true;
    this.fxBlade.node.rotation = (Math.random() - 0.5) * 2 * 40;
    this.fxBlade.play('blade');
    this.unscheduleAllCallbacks();
    this.node.stopAllActions();
    this.waveMng.hitFoe();
    this.player.addKills();

    if (--this.hp > 0) {
      this.isInvincible = true;
      this.scheduleOnce(this.invincible, this.bloodDuration);
    } else {
      this.isAlive = false;
      this.scheduleOnce(this.corpse, this.bloodDuration);
      this.waveMng.killFoe();
    }
  },
  invincible: function invincible() {
    this.fxBlood.node.active = false;
    this.isMoving = true;
    var blink = cc.blink(1, 6);
    var callback = cc.callFunc(this.onInvincibleEnd, this);
    this.anim.node.runAction(cc.sequence(blink, callback));
  },
  onInvincibleEnd: function onInvincibleEnd() {
    this.isInvincible = false;
  },
  corpse: function corpse() {
    this.anim.play('corpse');
    this.fxBlood.node.active = false;
    this.scheduleOnce(this.recycle, this.corpseDuration);
  },
  recycle: function recycle() {
    this.waveMng.despawnFoe(this);
  }
});

cc._RF.pop();