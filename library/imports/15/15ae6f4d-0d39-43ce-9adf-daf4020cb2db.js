"use strict";
cc._RF.push(module, '15ae69NDTlDzprf2vQCDLLb', 'PoolMng');
// scripts/PoolMng.js

"use strict";

var NodePool = require('NodePool');

var FoeType = require('Types').FoeType;

var ProjectileType = require('Types').ProjectileType;

cc.Class({
  "extends": cc.Component,
  properties: {
    foePools: {
      "default": [],
      type: NodePool
    },
    projectilePools: {
      "default": [],
      type: NodePool
    }
  },
  // use this for initialization
  init: function init() {
    for (var i = 0; i < this.foePools.length; ++i) {
      this.foePools[i].init();
    }

    for (var _i = 0; _i < this.projectilePools.length; ++_i) {
      this.projectilePools[_i].init();
    }
  },
  requestFoe: function requestFoe(foeType) {
    var thePool = this.foePools[foeType];

    if (thePool.idx >= 0) {
      return thePool.request();
    } else {
      return null;
    }
  },
  returnFoe: function returnFoe(foeType, obj) {
    var thePool = this.foePools[foeType];

    if (thePool.idx < thePool.size) {
      thePool["return"](obj);
    } else {
      cc.log('Return obj to a full pool, something has gone wrong');
      return;
    }
  },
  requestProjectile: function requestProjectile(type) {
    var thePool = this.projectilePools[type];

    if (thePool.idx >= 0) {
      return thePool.request();
    } else {
      return null;
    }
  },
  returnProjectile: function returnProjectile(type, obj) {
    var thePool = this.projectilePools[type];

    if (thePool.idx < thePool.size) {
      thePool["return"](obj);
    } else {
      cc.log('Return obj to a full pool, something has gone wrong');
      return;
    }
  }
});

cc._RF.pop();