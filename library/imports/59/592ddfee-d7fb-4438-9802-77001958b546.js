"use strict";
cc._RF.push(module, '592dd/u1/tEOJgCdwAZWLVG', 'KillDisplay');
// scripts/Render/KillDisplay.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {
    labelKills: cc.Label,
    anim: cc.Animation
  },
  playKill: function playKill(kills) {
    this.node.active = true;
    this.labelKills.string = kills;
    this.anim.play('kill-pop');
  },
  hide: function hide() {
    this.node.active = false;
  } // called every frame, uncomment this function to activate update callback
  // update: function (dt) {
  // },

});

cc._RF.pop();