"use strict";
cc._RF.push(module, 'd6ff4kVMSdOh7u6WwhErR6E', 'AnimHelper');
// scripts/AnimHelper.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {
    particleToPlay: cc.ParticleSystem,
    finishHandler: cc.Component.EventHandler,
    fireHandler: cc.Component.EventHandler
  },
  // use this for initialization
  playParticle: function playParticle() {
    if (this.particleToPlay) {
      this.particleToPlay.resetSystem();
    }
  },
  fire: function fire() {
    cc.Component.EventHandler.emitEvents([this.fireHandler]);
  },
  finish: function finish() {
    cc.Component.EventHandler.emitEvents([this.finishHandler]);
  }
});

cc._RF.pop();