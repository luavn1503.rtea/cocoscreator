"use strict";
cc._RF.push(module, '61165e1leJB+4dbkFmx1myT', 'SortMng');
// scripts/Render/SortMng.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {},
  // use this for initialization
  init: function init() {
    this.frameCount = 0;
  },
  // called every frame, uncomment this function to activate update callback
  update: function update(dt) {
    if (++this.frameCount % 6 === 0) {
      this.sortChildrenByY();
    }
  },
  sortChildrenByY: function sortChildrenByY() {
    var listToSort = this.node.children.slice();
    listToSort.sort(function (a, b) {
      return b.y - a.y;
    });

    for (var i = 0; i < listToSort.length; ++i) {
      var node = listToSort[i];

      if (node.active) {
        node.setSiblingIndex(i);
      }
    }
  }
});

cc._RF.pop();