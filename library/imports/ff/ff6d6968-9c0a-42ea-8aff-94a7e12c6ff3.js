"use strict";
cc._RF.push(module, 'ff6d6lonApC6or/lKfhLG/z', 'Spawn');
// scripts/Actors/Spawn.js

"use strict";

var FoeType = require('Types').FoeType;

var Spawn = cc.Class({
  name: 'Spawn',
  properties: {
    foeType: {
      "default": FoeType.Foe0,
      type: FoeType
    },
    total: 0,
    spawnInterval: 0,
    isCompany: false
  },
  ctor: function ctor() {
    this.spawned = 0;
    this.finished = false;
  },
  spawn: function spawn(poolMng) {
    if (this.spawned >= this.total) {
      return;
    }

    var newFoe = poolMng.requestFoe(this.foeType);

    if (newFoe) {
      this.spawned++;

      if (this.spawned === this.total) {
        this.finished = true;
      }

      return newFoe;
    } else {
      cc.log('max foe count reached, will delay spawn');
      return null;
    }
  }
});
module.exports = Spawn;

cc._RF.pop();