
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/__qc_index__.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}
require('./assets/migration/use_v2.1-2.2.1_cc.Toggle_event');
require('./assets/migration/use_v2.1.x_cc.Action');
require('./assets/scripts/Actors/BossMng');
require('./assets/scripts/Actors/Foe');
require('./assets/scripts/Actors/Move');
require('./assets/scripts/Actors/Player');
require('./assets/scripts/Actors/Projectile');
require('./assets/scripts/Actors/Spawn');
require('./assets/scripts/Actors/WaveMng');
require('./assets/scripts/AnimHelper');
require('./assets/scripts/Game');
require('./assets/scripts/NodePool');
require('./assets/scripts/PoolMng');
require('./assets/scripts/Render/ComboDisplay');
require('./assets/scripts/Render/KillDisplay');
require('./assets/scripts/Render/PlayerFX');
require('./assets/scripts/Render/SortMng');
require('./assets/scripts/ShowMask');
require('./assets/scripts/Types');
require('./assets/scripts/UI/BossProgress');
require('./assets/scripts/UI/ButtonScaler');
require('./assets/scripts/UI/DeathUI');
require('./assets/scripts/UI/GameOverUI');
require('./assets/scripts/UI/HomeUI');
require('./assets/scripts/UI/InGameUI');
require('./assets/scripts/UI/PauseUI');
require('./assets/scripts/UI/WaveProgress');
require('./assets/scripts/UI/WaveUI');

                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();