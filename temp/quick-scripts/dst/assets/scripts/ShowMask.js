
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/ShowMask.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '78c7dPnl5JF/p5GDHEQRYOC', 'ShowMask');
// scripts/ShowMask.js

"use strict";

// since mask is hidden in the scene (for editable),
// we use this script to show it at runtime
cc.Class({
  "extends": cc.Component,
  start: function start() {
    this.getComponent(cc.Sprite).enabled = true;
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcU2hvd01hc2suanMiXSwibmFtZXMiOlsiY2MiLCJDbGFzcyIsIkNvbXBvbmVudCIsInN0YXJ0IiwiZ2V0Q29tcG9uZW50IiwiU3ByaXRlIiwiZW5hYmxlZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0FBLEVBQUUsQ0FBQ0MsS0FBSCxDQUFTO0FBQ0wsYUFBU0QsRUFBRSxDQUFDRSxTQURQO0FBR0xDLEVBQUFBLEtBSEssbUJBR0k7QUFDTCxTQUFLQyxZQUFMLENBQWtCSixFQUFFLENBQUNLLE1BQXJCLEVBQTZCQyxPQUE3QixHQUF1QyxJQUF2QztBQUNIO0FBTEksQ0FBVCIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiLy8gc2luY2UgbWFzayBpcyBoaWRkZW4gaW4gdGhlIHNjZW5lIChmb3IgZWRpdGFibGUpLFxuLy8gd2UgdXNlIHRoaXMgc2NyaXB0IHRvIHNob3cgaXQgYXQgcnVudGltZVxuY2MuQ2xhc3Moe1xuICAgIGV4dGVuZHM6IGNjLkNvbXBvbmVudCxcbiAgIFxuICAgIHN0YXJ0ICgpIHtcbiAgICAgICAgdGhpcy5nZXRDb21wb25lbnQoY2MuU3ByaXRlKS5lbmFibGVkID0gdHJ1ZTtcbiAgICB9LFxufSk7XG4iXX0=