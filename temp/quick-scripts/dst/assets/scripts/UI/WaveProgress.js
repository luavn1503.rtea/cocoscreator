
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/UI/WaveProgress.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'a90aa7G1q5ANJGKlnUcA6SL', 'WaveProgress');
// scripts/UI/WaveProgress.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {
    bar: cc.ProgressBar,
    head: cc.Node,
    lerpDuration: 0
  },
  onLoad: function onLoad() {},
  init: function init(waveMng) {
    this.waveMng = waveMng;
    this.bar.progress = 0;
    this.curProgress = 0;
    this.destProgress = 0;
    this.timer = 0;
    this.isLerping = false;
  },
  updateProgress: function updateProgress(progress) {
    this.curProgress = this.bar.progress;
    this.destProgress = progress;
    this.timer = 0;
    this.isLerping = true;
  },
  update: function update(dt) {
    if (this.isLerping === false) {
      return;
    }

    this.timer += dt;

    if (this.timer >= this.lerpDuration) {
      this.timer = this.lerpDuration;
      this.isLerping = false;
    }

    this.bar.progress = cc.misc.lerp(this.curProgress, this.destProgress, this.timer / this.lerpDuration);
    var headPosX = this.bar.barSprite.node.width * this.bar.progress;
    this.head.x = headPosX;
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcVUlcXFdhdmVQcm9ncmVzcy5qcyJdLCJuYW1lcyI6WyJjYyIsIkNsYXNzIiwiQ29tcG9uZW50IiwicHJvcGVydGllcyIsImJhciIsIlByb2dyZXNzQmFyIiwiaGVhZCIsIk5vZGUiLCJsZXJwRHVyYXRpb24iLCJvbkxvYWQiLCJpbml0Iiwid2F2ZU1uZyIsInByb2dyZXNzIiwiY3VyUHJvZ3Jlc3MiLCJkZXN0UHJvZ3Jlc3MiLCJ0aW1lciIsImlzTGVycGluZyIsInVwZGF0ZVByb2dyZXNzIiwidXBkYXRlIiwiZHQiLCJtaXNjIiwibGVycCIsImhlYWRQb3NYIiwiYmFyU3ByaXRlIiwibm9kZSIsIndpZHRoIiwieCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQUEsRUFBRSxDQUFDQyxLQUFILENBQVM7QUFDTCxhQUFTRCxFQUFFLENBQUNFLFNBRFA7QUFHTEMsRUFBQUEsVUFBVSxFQUFFO0FBQ1JDLElBQUFBLEdBQUcsRUFBRUosRUFBRSxDQUFDSyxXQURBO0FBRVJDLElBQUFBLElBQUksRUFBRU4sRUFBRSxDQUFDTyxJQUZEO0FBR1JDLElBQUFBLFlBQVksRUFBRTtBQUhOLEdBSFA7QUFTTEMsRUFBQUEsTUFUSyxvQkFTSyxDQUNULENBVkk7QUFZTEMsRUFBQUEsSUFaSyxnQkFZQ0MsT0FaRCxFQVlVO0FBQ1gsU0FBS0EsT0FBTCxHQUFlQSxPQUFmO0FBQ0EsU0FBS1AsR0FBTCxDQUFTUSxRQUFULEdBQW9CLENBQXBCO0FBQ0EsU0FBS0MsV0FBTCxHQUFtQixDQUFuQjtBQUNBLFNBQUtDLFlBQUwsR0FBb0IsQ0FBcEI7QUFDQSxTQUFLQyxLQUFMLEdBQWEsQ0FBYjtBQUNBLFNBQUtDLFNBQUwsR0FBaUIsS0FBakI7QUFDSCxHQW5CSTtBQXFCTEMsRUFBQUEsY0FyQkssMEJBcUJXTCxRQXJCWCxFQXFCcUI7QUFDdEIsU0FBS0MsV0FBTCxHQUFtQixLQUFLVCxHQUFMLENBQVNRLFFBQTVCO0FBQ0EsU0FBS0UsWUFBTCxHQUFvQkYsUUFBcEI7QUFDQSxTQUFLRyxLQUFMLEdBQWEsQ0FBYjtBQUNBLFNBQUtDLFNBQUwsR0FBaUIsSUFBakI7QUFDSCxHQTFCSTtBQTRCTEUsRUFBQUEsTUFBTSxFQUFFLGdCQUFVQyxFQUFWLEVBQWM7QUFDbEIsUUFBSSxLQUFLSCxTQUFMLEtBQW1CLEtBQXZCLEVBQThCO0FBQzFCO0FBQ0g7O0FBQ0QsU0FBS0QsS0FBTCxJQUFjSSxFQUFkOztBQUNBLFFBQUksS0FBS0osS0FBTCxJQUFjLEtBQUtQLFlBQXZCLEVBQXFDO0FBQ2pDLFdBQUtPLEtBQUwsR0FBYSxLQUFLUCxZQUFsQjtBQUNBLFdBQUtRLFNBQUwsR0FBaUIsS0FBakI7QUFDSDs7QUFDRCxTQUFLWixHQUFMLENBQVNRLFFBQVQsR0FBb0JaLEVBQUUsQ0FBQ29CLElBQUgsQ0FBUUMsSUFBUixDQUFhLEtBQUtSLFdBQWxCLEVBQStCLEtBQUtDLFlBQXBDLEVBQWtELEtBQUtDLEtBQUwsR0FBVyxLQUFLUCxZQUFsRSxDQUFwQjtBQUNBLFFBQUljLFFBQVEsR0FBRyxLQUFLbEIsR0FBTCxDQUFTbUIsU0FBVCxDQUFtQkMsSUFBbkIsQ0FBd0JDLEtBQXhCLEdBQWdDLEtBQUtyQixHQUFMLENBQVNRLFFBQXhEO0FBQ0EsU0FBS04sSUFBTCxDQUFVb0IsQ0FBVixHQUFjSixRQUFkO0FBQ0g7QUF4Q0ksQ0FBVCIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiY2MuQ2xhc3Moe1xuICAgIGV4dGVuZHM6IGNjLkNvbXBvbmVudCxcblxuICAgIHByb3BlcnRpZXM6IHtcbiAgICAgICAgYmFyOiBjYy5Qcm9ncmVzc0JhcixcbiAgICAgICAgaGVhZDogY2MuTm9kZSxcbiAgICAgICAgbGVycER1cmF0aW9uOiAwXG4gICAgfSxcblxuICAgIG9uTG9hZCAoKSB7XG4gICAgfSxcblxuICAgIGluaXQgKHdhdmVNbmcpIHtcbiAgICAgICAgdGhpcy53YXZlTW5nID0gd2F2ZU1uZztcbiAgICAgICAgdGhpcy5iYXIucHJvZ3Jlc3MgPSAwO1xuICAgICAgICB0aGlzLmN1clByb2dyZXNzID0gMDtcbiAgICAgICAgdGhpcy5kZXN0UHJvZ3Jlc3MgPSAwO1xuICAgICAgICB0aGlzLnRpbWVyID0gMDtcbiAgICAgICAgdGhpcy5pc0xlcnBpbmcgPSBmYWxzZTtcbiAgICB9LFxuXG4gICAgdXBkYXRlUHJvZ3Jlc3MgKHByb2dyZXNzKSB7XG4gICAgICAgIHRoaXMuY3VyUHJvZ3Jlc3MgPSB0aGlzLmJhci5wcm9ncmVzcztcbiAgICAgICAgdGhpcy5kZXN0UHJvZ3Jlc3MgPSBwcm9ncmVzcztcbiAgICAgICAgdGhpcy50aW1lciA9IDA7XG4gICAgICAgIHRoaXMuaXNMZXJwaW5nID0gdHJ1ZTtcbiAgICB9LFxuXG4gICAgdXBkYXRlOiBmdW5jdGlvbiAoZHQpIHtcbiAgICAgICAgaWYgKHRoaXMuaXNMZXJwaW5nID09PSBmYWxzZSkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMudGltZXIgKz0gZHQ7XG4gICAgICAgIGlmICh0aGlzLnRpbWVyID49IHRoaXMubGVycER1cmF0aW9uKSB7XG4gICAgICAgICAgICB0aGlzLnRpbWVyID0gdGhpcy5sZXJwRHVyYXRpb247XG4gICAgICAgICAgICB0aGlzLmlzTGVycGluZyA9IGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuYmFyLnByb2dyZXNzID0gY2MubWlzYy5sZXJwKHRoaXMuY3VyUHJvZ3Jlc3MsIHRoaXMuZGVzdFByb2dyZXNzLCB0aGlzLnRpbWVyL3RoaXMubGVycER1cmF0aW9uKTtcbiAgICAgICAgbGV0IGhlYWRQb3NYID0gdGhpcy5iYXIuYmFyU3ByaXRlLm5vZGUud2lkdGggKiB0aGlzLmJhci5wcm9ncmVzcztcbiAgICAgICAgdGhpcy5oZWFkLnggPSBoZWFkUG9zWDtcbiAgICB9LFxufSk7XG4iXX0=