
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/UI/WaveUI.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '389b4yNsLZD8oJXlec0Kfzr', 'WaveUI');
// scripts/UI/WaveUI.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {
    labelWave: cc.Label,
    anim: cc.Animation
  },
  // use this for initialization
  onLoad: function onLoad() {},
  show: function show(num) {
    this.labelWave.string = num;
    this.anim.play('wave-pop');
  },
  hide: function hide() {
    this.node.active = false;
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcVUlcXFdhdmVVSS5qcyJdLCJuYW1lcyI6WyJjYyIsIkNsYXNzIiwiQ29tcG9uZW50IiwicHJvcGVydGllcyIsImxhYmVsV2F2ZSIsIkxhYmVsIiwiYW5pbSIsIkFuaW1hdGlvbiIsIm9uTG9hZCIsInNob3ciLCJudW0iLCJzdHJpbmciLCJwbGF5IiwiaGlkZSIsIm5vZGUiLCJhY3RpdmUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUFBLEVBQUUsQ0FBQ0MsS0FBSCxDQUFTO0FBQ0wsYUFBU0QsRUFBRSxDQUFDRSxTQURQO0FBR0xDLEVBQUFBLFVBQVUsRUFBRTtBQUNSQyxJQUFBQSxTQUFTLEVBQUVKLEVBQUUsQ0FBQ0ssS0FETjtBQUVSQyxJQUFBQSxJQUFJLEVBQUVOLEVBQUUsQ0FBQ087QUFGRCxHQUhQO0FBUUw7QUFDQUMsRUFBQUEsTUFBTSxFQUFFLGtCQUFZLENBRW5CLENBWEk7QUFhTEMsRUFBQUEsSUFiSyxnQkFhQ0MsR0FiRCxFQWFNO0FBQ1AsU0FBS04sU0FBTCxDQUFlTyxNQUFmLEdBQXdCRCxHQUF4QjtBQUNBLFNBQUtKLElBQUwsQ0FBVU0sSUFBVixDQUFlLFVBQWY7QUFDSCxHQWhCSTtBQWtCTEMsRUFBQUEsSUFsQkssa0JBa0JHO0FBQ0osU0FBS0MsSUFBTCxDQUFVQyxNQUFWLEdBQW1CLEtBQW5CO0FBQ0g7QUFwQkksQ0FBVCIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiY2MuQ2xhc3Moe1xuICAgIGV4dGVuZHM6IGNjLkNvbXBvbmVudCxcblxuICAgIHByb3BlcnRpZXM6IHtcbiAgICAgICAgbGFiZWxXYXZlOiBjYy5MYWJlbCxcbiAgICAgICAgYW5pbTogY2MuQW5pbWF0aW9uXG4gICAgfSxcblxuICAgIC8vIHVzZSB0aGlzIGZvciBpbml0aWFsaXphdGlvblxuICAgIG9uTG9hZDogZnVuY3Rpb24gKCkge1xuXG4gICAgfSxcblxuICAgIHNob3cgKG51bSkge1xuICAgICAgICB0aGlzLmxhYmVsV2F2ZS5zdHJpbmcgPSBudW07XG4gICAgICAgIHRoaXMuYW5pbS5wbGF5KCd3YXZlLXBvcCcpO1xuICAgIH0sXG5cbiAgICBoaWRlICgpIHtcbiAgICAgICAgdGhpcy5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgIH1cbn0pO1xuIl19