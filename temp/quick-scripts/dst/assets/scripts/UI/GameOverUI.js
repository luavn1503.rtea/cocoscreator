
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/UI/GameOverUI.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'c3ee4ElVWtB2Lzir0h5v/ow', 'GameOverUI');
// scripts/UI/GameOverUI.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {},
  init: function init(game) {
    this.game = game;
    this.hide();
  },
  // use this for initialization
  show: function show() {
    this.node.setPosition(0, 0);
  },
  hide: function hide() {
    this.node.x = 3000;
  },
  restart: function restart() {
    this.game.restart();
  },
  onLoad: function onLoad() {},
  startGame: function startGame() {
    cc.eventManager.pauseTarget(this.btnGroup, true);
    cc.director.loadScene('StartGame');
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcVUlcXEdhbWVPdmVyVUkuanMiXSwibmFtZXMiOlsiY2MiLCJDbGFzcyIsIkNvbXBvbmVudCIsInByb3BlcnRpZXMiLCJpbml0IiwiZ2FtZSIsImhpZGUiLCJzaG93Iiwibm9kZSIsInNldFBvc2l0aW9uIiwieCIsInJlc3RhcnQiLCJvbkxvYWQiLCJzdGFydEdhbWUiLCJldmVudE1hbmFnZXIiLCJwYXVzZVRhcmdldCIsImJ0bkdyb3VwIiwiZGlyZWN0b3IiLCJsb2FkU2NlbmUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUFBLEVBQUUsQ0FBQ0MsS0FBSCxDQUFTO0FBQ0wsYUFBU0QsRUFBRSxDQUFDRSxTQURQO0FBR0xDLEVBQUFBLFVBQVUsRUFBRSxFQUhQO0FBTUxDLEVBQUFBLElBTkssZ0JBTUNDLElBTkQsRUFNTztBQUNSLFNBQUtBLElBQUwsR0FBWUEsSUFBWjtBQUNBLFNBQUtDLElBQUw7QUFDSCxHQVRJO0FBV0w7QUFDQUMsRUFBQUEsSUFaSyxrQkFZRztBQUNKLFNBQUtDLElBQUwsQ0FBVUMsV0FBVixDQUFzQixDQUF0QixFQUF5QixDQUF6QjtBQUNILEdBZEk7QUFnQkxILEVBQUFBLElBaEJLLGtCQWdCRztBQUNKLFNBQUtFLElBQUwsQ0FBVUUsQ0FBVixHQUFjLElBQWQ7QUFDSCxHQWxCSTtBQW9CTEMsRUFBQUEsT0FwQksscUJBb0JNO0FBQ1AsU0FBS04sSUFBTCxDQUFVTSxPQUFWO0FBQ0gsR0F0Qkk7QUF3QkxDLEVBQUFBLE1BQU0sRUFBRSxrQkFBWSxDQUVuQixDQTFCSTtBQTJCTEMsRUFBQUEsU0FBUyxFQUFFLHFCQUFZO0FBQ25CYixJQUFBQSxFQUFFLENBQUNjLFlBQUgsQ0FBZ0JDLFdBQWhCLENBQTRCLEtBQUtDLFFBQWpDLEVBQTJDLElBQTNDO0FBQ0FoQixJQUFBQSxFQUFFLENBQUNpQixRQUFILENBQVlDLFNBQVosQ0FBc0IsV0FBdEI7QUFDSDtBQTlCSSxDQUFUIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJjYy5DbGFzcyh7XG4gICAgZXh0ZW5kczogY2MuQ29tcG9uZW50LFxuXG4gICAgcHJvcGVydGllczoge1xuICAgIH0sXG5cbiAgICBpbml0IChnYW1lKSB7XG4gICAgICAgIHRoaXMuZ2FtZSA9IGdhbWU7XG4gICAgICAgIHRoaXMuaGlkZSgpO1xuICAgIH0sXG5cbiAgICAvLyB1c2UgdGhpcyBmb3IgaW5pdGlhbGl6YXRpb25cbiAgICBzaG93ICgpIHtcbiAgICAgICAgdGhpcy5ub2RlLnNldFBvc2l0aW9uKDAsIDApO1xuICAgIH0sXG5cbiAgICBoaWRlICgpIHtcbiAgICAgICAgdGhpcy5ub2RlLnggPSAzMDAwO1xuICAgIH0sXG5cbiAgICByZXN0YXJ0ICgpIHtcbiAgICAgICAgdGhpcy5nYW1lLnJlc3RhcnQoKTtcbiAgICB9LFxuXG4gICAgb25Mb2FkOiBmdW5jdGlvbiAoKSB7XG5cbiAgICB9LFxuICAgIHN0YXJ0R2FtZTogZnVuY3Rpb24gKCkge1xuICAgICAgICBjYy5ldmVudE1hbmFnZXIucGF1c2VUYXJnZXQodGhpcy5idG5Hcm91cCwgdHJ1ZSk7XG4gICAgICAgIGNjLmRpcmVjdG9yLmxvYWRTY2VuZSgnU3RhcnRHYW1lJyk7XG4gICAgfVxufSk7XG4iXX0=