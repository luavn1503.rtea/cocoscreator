
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/UI/DeathUI.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '0966f3/svtKzIRd+HwG3Kyd', 'DeathUI');
// scripts/UI/DeathUI.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {},
  init: function init(game) {
    this.game = game;
    this.hide();
  },
  show: function show() {
    this.node.setPosition(0, 0);
  },
  hide: function hide() {
    this.node.x = 3000;
  },
  revive: function revive() {
    this.game.revive();
  },
  gameover: function gameover() {
    this.game.gameOver();
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcVUlcXERlYXRoVUkuanMiXSwibmFtZXMiOlsiY2MiLCJDbGFzcyIsIkNvbXBvbmVudCIsInByb3BlcnRpZXMiLCJpbml0IiwiZ2FtZSIsImhpZGUiLCJzaG93Iiwibm9kZSIsInNldFBvc2l0aW9uIiwieCIsInJldml2ZSIsImdhbWVvdmVyIiwiZ2FtZU92ZXIiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUFBLEVBQUUsQ0FBQ0MsS0FBSCxDQUFTO0FBQ0wsYUFBU0QsRUFBRSxDQUFDRSxTQURQO0FBR0xDLEVBQUFBLFVBQVUsRUFBRSxFQUhQO0FBT0xDLEVBQUFBLElBUEssZ0JBT0NDLElBUEQsRUFPTztBQUNSLFNBQUtBLElBQUwsR0FBWUEsSUFBWjtBQUNBLFNBQUtDLElBQUw7QUFDSCxHQVZJO0FBWUxDLEVBQUFBLElBWkssa0JBWUc7QUFDSixTQUFLQyxJQUFMLENBQVVDLFdBQVYsQ0FBc0IsQ0FBdEIsRUFBeUIsQ0FBekI7QUFDSCxHQWRJO0FBZ0JMSCxFQUFBQSxJQWhCSyxrQkFnQkc7QUFDSixTQUFLRSxJQUFMLENBQVVFLENBQVYsR0FBYyxJQUFkO0FBQ0gsR0FsQkk7QUFvQkxDLEVBQUFBLE1BcEJLLG9CQW9CSztBQUNOLFNBQUtOLElBQUwsQ0FBVU0sTUFBVjtBQUNILEdBdEJJO0FBd0JMQyxFQUFBQSxRQXhCSyxzQkF3Qk87QUFDUixTQUFLUCxJQUFMLENBQVVRLFFBQVY7QUFDSDtBQTFCSSxDQUFUIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJjYy5DbGFzcyh7XG4gICAgZXh0ZW5kczogY2MuQ29tcG9uZW50LFxuXG4gICAgcHJvcGVydGllczoge1xuICAgICAgICBcbiAgICB9LFxuXG4gICAgaW5pdCAoZ2FtZSkge1xuICAgICAgICB0aGlzLmdhbWUgPSBnYW1lO1xuICAgICAgICB0aGlzLmhpZGUoKTtcbiAgICB9LFxuXG4gICAgc2hvdyAoKSB7XG4gICAgICAgIHRoaXMubm9kZS5zZXRQb3NpdGlvbigwLCAwKTtcbiAgICB9LFxuXG4gICAgaGlkZSAoKSB7XG4gICAgICAgIHRoaXMubm9kZS54ID0gMzAwMDtcbiAgICB9LFxuXG4gICAgcmV2aXZlICgpIHtcbiAgICAgICAgdGhpcy5nYW1lLnJldml2ZSgpO1xuICAgIH0sXG5cbiAgICBnYW1lb3ZlciAoKSB7XG4gICAgICAgIHRoaXMuZ2FtZS5nYW1lT3ZlcigpO1xuICAgIH0sXG4gIFxufSk7XG4iXX0=