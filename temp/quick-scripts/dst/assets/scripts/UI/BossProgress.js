
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/UI/BossProgress.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '178aaufFWBMjKEMUnBNqyHl', 'BossProgress');
// scripts/UI/BossProgress.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {
    fxParticle: cc.ParticleSystem,
    anim: cc.Animation
  },
  // use this for initialization
  init: function init(waveMng) {
    this.waveMng = waveMng;
  },
  show: function show() {
    this.node.active = true;
    this.anim.play('turn-red');
  },
  hide: function hide() {
    this.node.active = false;
  },
  showParticle: function showParticle() {
    this.fxParticle.resetSystem();
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcVUlcXEJvc3NQcm9ncmVzcy5qcyJdLCJuYW1lcyI6WyJjYyIsIkNsYXNzIiwiQ29tcG9uZW50IiwicHJvcGVydGllcyIsImZ4UGFydGljbGUiLCJQYXJ0aWNsZVN5c3RlbSIsImFuaW0iLCJBbmltYXRpb24iLCJpbml0Iiwid2F2ZU1uZyIsInNob3ciLCJub2RlIiwiYWN0aXZlIiwicGxheSIsImhpZGUiLCJzaG93UGFydGljbGUiLCJyZXNldFN5c3RlbSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQUEsRUFBRSxDQUFDQyxLQUFILENBQVM7QUFDTCxhQUFTRCxFQUFFLENBQUNFLFNBRFA7QUFHTEMsRUFBQUEsVUFBVSxFQUFFO0FBQ1JDLElBQUFBLFVBQVUsRUFBRUosRUFBRSxDQUFDSyxjQURQO0FBRVJDLElBQUFBLElBQUksRUFBRU4sRUFBRSxDQUFDTztBQUZELEdBSFA7QUFRTDtBQUNBQyxFQUFBQSxJQVRLLGdCQVNDQyxPQVRELEVBU1U7QUFDWCxTQUFLQSxPQUFMLEdBQWVBLE9BQWY7QUFDSCxHQVhJO0FBYUxDLEVBQUFBLElBYkssa0JBYUc7QUFDSixTQUFLQyxJQUFMLENBQVVDLE1BQVYsR0FBbUIsSUFBbkI7QUFDQSxTQUFLTixJQUFMLENBQVVPLElBQVYsQ0FBZSxVQUFmO0FBQ0gsR0FoQkk7QUFrQkxDLEVBQUFBLElBbEJLLGtCQWtCRztBQUNKLFNBQUtILElBQUwsQ0FBVUMsTUFBVixHQUFtQixLQUFuQjtBQUNILEdBcEJJO0FBc0JMRyxFQUFBQSxZQXRCSywwQkFzQlc7QUFDWixTQUFLWCxVQUFMLENBQWdCWSxXQUFoQjtBQUNIO0FBeEJJLENBQVQiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImNjLkNsYXNzKHtcbiAgICBleHRlbmRzOiBjYy5Db21wb25lbnQsXG5cbiAgICBwcm9wZXJ0aWVzOiB7XG4gICAgICAgIGZ4UGFydGljbGU6IGNjLlBhcnRpY2xlU3lzdGVtLFxuICAgICAgICBhbmltOiBjYy5BbmltYXRpb25cbiAgICB9LFxuXG4gICAgLy8gdXNlIHRoaXMgZm9yIGluaXRpYWxpemF0aW9uXG4gICAgaW5pdCAod2F2ZU1uZykge1xuICAgICAgICB0aGlzLndhdmVNbmcgPSB3YXZlTW5nO1xuICAgIH0sXG5cbiAgICBzaG93ICgpIHtcbiAgICAgICAgdGhpcy5ub2RlLmFjdGl2ZSA9IHRydWU7XG4gICAgICAgIHRoaXMuYW5pbS5wbGF5KCd0dXJuLXJlZCcpO1xuICAgIH0sXG5cbiAgICBoaWRlICgpIHtcbiAgICAgICAgdGhpcy5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgIH0sXG5cbiAgICBzaG93UGFydGljbGUgKCkge1xuICAgICAgICB0aGlzLmZ4UGFydGljbGUucmVzZXRTeXN0ZW0oKTtcbiAgICB9XG4gICAgXG59KTtcbiJdfQ==