
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/UI/HomeUI.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'aa77bcy1MlA3bRL8zpupFoD', 'HomeUI');
// scripts/UI/HomeUI.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {
    menuAnim: {
      "default": null,
      type: cc.Animation
    },
    menuParticle: {
      "default": null,
      type: cc.ParticleSystem
    },
    btnGroup: {
      "default": null,
      type: cc.Node
    }
  },
  // use this for initialization
  onLoad: function onLoad() {},
  start: function start() {
    cc.eventManager.pauseTarget(this.btnGroup, true);
    this.scheduleOnce(function () {
      this.menuAnim.play();
      this.menuParticle.enabled = false;
    }.bind(this), 2);
  },
  showParticle: function showParticle() {
    this.menuParticle.enabled = true;
  },
  enableButtons: function enableButtons() {
    cc.eventManager.resumeTarget(this.btnGroup, true);
  },
  playGame: function playGame() {
    cc.eventManager.pauseTarget(this.btnGroup, true);
    cc.director.loadScene('PlayGame');
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcVUlcXEhvbWVVSS5qcyJdLCJuYW1lcyI6WyJjYyIsIkNsYXNzIiwiQ29tcG9uZW50IiwicHJvcGVydGllcyIsIm1lbnVBbmltIiwidHlwZSIsIkFuaW1hdGlvbiIsIm1lbnVQYXJ0aWNsZSIsIlBhcnRpY2xlU3lzdGVtIiwiYnRuR3JvdXAiLCJOb2RlIiwib25Mb2FkIiwic3RhcnQiLCJldmVudE1hbmFnZXIiLCJwYXVzZVRhcmdldCIsInNjaGVkdWxlT25jZSIsInBsYXkiLCJlbmFibGVkIiwiYmluZCIsInNob3dQYXJ0aWNsZSIsImVuYWJsZUJ1dHRvbnMiLCJyZXN1bWVUYXJnZXQiLCJwbGF5R2FtZSIsImRpcmVjdG9yIiwibG9hZFNjZW5lIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBQSxFQUFFLENBQUNDLEtBQUgsQ0FBUztBQUNMLGFBQVNELEVBQUUsQ0FBQ0UsU0FEUDtBQUdMQyxFQUFBQSxVQUFVLEVBQUU7QUFDUkMsSUFBQUEsUUFBUSxFQUFFO0FBQ04saUJBQVMsSUFESDtBQUVOQyxNQUFBQSxJQUFJLEVBQUVMLEVBQUUsQ0FBQ007QUFGSCxLQURGO0FBS1JDLElBQUFBLFlBQVksRUFBRTtBQUNWLGlCQUFTLElBREM7QUFFVkYsTUFBQUEsSUFBSSxFQUFFTCxFQUFFLENBQUNRO0FBRkMsS0FMTjtBQVNSQyxJQUFBQSxRQUFRLEVBQUU7QUFDTixpQkFBUyxJQURIO0FBRU5KLE1BQUFBLElBQUksRUFBRUwsRUFBRSxDQUFDVTtBQUZIO0FBVEYsR0FIUDtBQW9CTDtBQUNBQyxFQUFBQSxNQUFNLEVBQUUsa0JBQVksQ0FDbkIsQ0F0Qkk7QUF3QkxDLEVBQUFBLEtBQUssRUFBRSxpQkFBWTtBQUNmWixJQUFBQSxFQUFFLENBQUNhLFlBQUgsQ0FBZ0JDLFdBQWhCLENBQTRCLEtBQUtMLFFBQWpDLEVBQTJDLElBQTNDO0FBQ0EsU0FBS00sWUFBTCxDQUFrQixZQUFXO0FBQ3pCLFdBQUtYLFFBQUwsQ0FBY1ksSUFBZDtBQUNBLFdBQUtULFlBQUwsQ0FBa0JVLE9BQWxCLEdBQTRCLEtBQTVCO0FBQ0gsS0FIaUIsQ0FHaEJDLElBSGdCLENBR1gsSUFIVyxDQUFsQixFQUdjLENBSGQ7QUFJSCxHQTlCSTtBQWdDTEMsRUFBQUEsWUFBWSxFQUFFLHdCQUFZO0FBQ3RCLFNBQUtaLFlBQUwsQ0FBa0JVLE9BQWxCLEdBQTRCLElBQTVCO0FBQ0gsR0FsQ0k7QUFvQ0xHLEVBQUFBLGFBQWEsRUFBRSx5QkFBWTtBQUN2QnBCLElBQUFBLEVBQUUsQ0FBQ2EsWUFBSCxDQUFnQlEsWUFBaEIsQ0FBNkIsS0FBS1osUUFBbEMsRUFBNEMsSUFBNUM7QUFDSCxHQXRDSTtBQXdDTGEsRUFBQUEsUUFBUSxFQUFFLG9CQUFZO0FBQ2xCdEIsSUFBQUEsRUFBRSxDQUFDYSxZQUFILENBQWdCQyxXQUFoQixDQUE0QixLQUFLTCxRQUFqQyxFQUEyQyxJQUEzQztBQUNBVCxJQUFBQSxFQUFFLENBQUN1QixRQUFILENBQVlDLFNBQVosQ0FBc0IsVUFBdEI7QUFDSDtBQTNDSSxDQUFUIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJjYy5DbGFzcyh7XG4gICAgZXh0ZW5kczogY2MuQ29tcG9uZW50LFxuXG4gICAgcHJvcGVydGllczoge1xuICAgICAgICBtZW51QW5pbToge1xuICAgICAgICAgICAgZGVmYXVsdDogbnVsbCxcbiAgICAgICAgICAgIHR5cGU6IGNjLkFuaW1hdGlvblxuICAgICAgICB9LFxuICAgICAgICBtZW51UGFydGljbGU6IHtcbiAgICAgICAgICAgIGRlZmF1bHQ6IG51bGwsXG4gICAgICAgICAgICB0eXBlOiBjYy5QYXJ0aWNsZVN5c3RlbVxuICAgICAgICB9LFxuICAgICAgICBidG5Hcm91cDoge1xuICAgICAgICAgICAgZGVmYXVsdDogbnVsbCxcbiAgICAgICAgICAgIHR5cGU6IGNjLk5vZGVcbiAgICAgICAgfVxuXG4gICAgICBcbiAgICB9LFxuXG4gICAgLy8gdXNlIHRoaXMgZm9yIGluaXRpYWxpemF0aW9uXG4gICAgb25Mb2FkOiBmdW5jdGlvbiAoKSB7XG4gICAgfSxcblxuICAgIHN0YXJ0OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGNjLmV2ZW50TWFuYWdlci5wYXVzZVRhcmdldCh0aGlzLmJ0bkdyb3VwLCB0cnVlKTtcbiAgICAgICAgdGhpcy5zY2hlZHVsZU9uY2UoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB0aGlzLm1lbnVBbmltLnBsYXkoKTtcbiAgICAgICAgICAgIHRoaXMubWVudVBhcnRpY2xlLmVuYWJsZWQgPSBmYWxzZTtcbiAgICAgICAgfS5iaW5kKHRoaXMpLCAyKTtcbiAgICB9LFxuXG4gICAgc2hvd1BhcnRpY2xlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHRoaXMubWVudVBhcnRpY2xlLmVuYWJsZWQgPSB0cnVlO1xuICAgIH0sXG5cbiAgICBlbmFibGVCdXR0b25zOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGNjLmV2ZW50TWFuYWdlci5yZXN1bWVUYXJnZXQodGhpcy5idG5Hcm91cCwgdHJ1ZSk7XG4gICAgfSxcblxuICAgIHBsYXlHYW1lOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGNjLmV2ZW50TWFuYWdlci5wYXVzZVRhcmdldCh0aGlzLmJ0bkdyb3VwLCB0cnVlKTtcbiAgICAgICAgY2MuZGlyZWN0b3IubG9hZFNjZW5lKCdQbGF5R2FtZScpO1xuICAgIH1cbn0pO1xuIl19