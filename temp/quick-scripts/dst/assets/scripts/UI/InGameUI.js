
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/UI/InGameUI.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '8df10iIWI5JgrcFs/5AlGtD', 'InGameUI');
// scripts/UI/InGameUI.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {
    waveUI: cc.Node,
    killDisplay: cc.Node,
    comboDisplay: cc.Node,
    pauseUI: cc.Node //waveProgress: cc.Node

  },
  // use this for initialization
  init: function init(game) {
    this.waveUI = this.waveUI.getComponent('WaveUI');
    this.waveUI.node.active = false;
    this.killDisplay = this.killDisplay.getComponent('KillDisplay');
    this.killDisplay.node.active = false;
    this.comboDisplay = this.comboDisplay.getComponent('ComboDisplay');
    this.comboDisplay.init(); // this.waveProgress = this.waveProgress.getComponent('WaveProgress');
    // this.waveProgress.init(game.waveMng);
  },
  showWave: function showWave(num) {
    this.waveUI.node.active = true;
    this.waveUI.show(num);
  },
  showKills: function showKills(num) {
    this.killDisplay.playKill(num);
  },
  addCombo: function addCombo() {
    this.comboDisplay.playCombo();
  },
  showPopupPauseUI: function showPopupPauseUI() {
    this.pauseUI.node.active = true;
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcVUlcXEluR2FtZVVJLmpzIl0sIm5hbWVzIjpbImNjIiwiQ2xhc3MiLCJDb21wb25lbnQiLCJwcm9wZXJ0aWVzIiwid2F2ZVVJIiwiTm9kZSIsImtpbGxEaXNwbGF5IiwiY29tYm9EaXNwbGF5IiwicGF1c2VVSSIsImluaXQiLCJnYW1lIiwiZ2V0Q29tcG9uZW50Iiwibm9kZSIsImFjdGl2ZSIsInNob3dXYXZlIiwibnVtIiwic2hvdyIsInNob3dLaWxscyIsInBsYXlLaWxsIiwiYWRkQ29tYm8iLCJwbGF5Q29tYm8iLCJzaG93UG9wdXBQYXVzZVVJIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBQSxFQUFFLENBQUNDLEtBQUgsQ0FBUztBQUNMLGFBQVNELEVBQUUsQ0FBQ0UsU0FEUDtBQUdMQyxFQUFBQSxVQUFVLEVBQUU7QUFDUkMsSUFBQUEsTUFBTSxFQUFFSixFQUFFLENBQUNLLElBREg7QUFFUkMsSUFBQUEsV0FBVyxFQUFFTixFQUFFLENBQUNLLElBRlI7QUFHUkUsSUFBQUEsWUFBWSxFQUFFUCxFQUFFLENBQUNLLElBSFQ7QUFJUkcsSUFBQUEsT0FBTyxFQUFFUixFQUFFLENBQUNLLElBSkosQ0FLUjs7QUFMUSxHQUhQO0FBV0w7QUFDQUksRUFBQUEsSUFaSyxnQkFZQ0MsSUFaRCxFQVlPO0FBQ1IsU0FBS04sTUFBTCxHQUFjLEtBQUtBLE1BQUwsQ0FBWU8sWUFBWixDQUF5QixRQUF6QixDQUFkO0FBQ0EsU0FBS1AsTUFBTCxDQUFZUSxJQUFaLENBQWlCQyxNQUFqQixHQUEwQixLQUExQjtBQUNBLFNBQUtQLFdBQUwsR0FBbUIsS0FBS0EsV0FBTCxDQUFpQkssWUFBakIsQ0FBOEIsYUFBOUIsQ0FBbkI7QUFDQSxTQUFLTCxXQUFMLENBQWlCTSxJQUFqQixDQUFzQkMsTUFBdEIsR0FBK0IsS0FBL0I7QUFDQSxTQUFLTixZQUFMLEdBQW9CLEtBQUtBLFlBQUwsQ0FBa0JJLFlBQWxCLENBQStCLGNBQS9CLENBQXBCO0FBQ0EsU0FBS0osWUFBTCxDQUFrQkUsSUFBbEIsR0FOUSxDQU9SO0FBQ0E7QUFFSCxHQXRCSTtBQXdCTEssRUFBQUEsUUF4Qkssb0JBd0JLQyxHQXhCTCxFQXdCVTtBQUNYLFNBQUtYLE1BQUwsQ0FBWVEsSUFBWixDQUFpQkMsTUFBakIsR0FBMEIsSUFBMUI7QUFDQSxTQUFLVCxNQUFMLENBQVlZLElBQVosQ0FBaUJELEdBQWpCO0FBQ0gsR0EzQkk7QUE2QkxFLEVBQUFBLFNBN0JLLHFCQTZCTUYsR0E3Qk4sRUE2Qlc7QUFDWixTQUFLVCxXQUFMLENBQWlCWSxRQUFqQixDQUEwQkgsR0FBMUI7QUFDSCxHQS9CSTtBQWlDTEksRUFBQUEsUUFqQ0ssc0JBaUNPO0FBQ1IsU0FBS1osWUFBTCxDQUFrQmEsU0FBbEI7QUFDSCxHQW5DSTtBQXFDTEMsRUFBQUEsZ0JBckNLLDhCQXFDYTtBQUNkLFNBQUtiLE9BQUwsQ0FBYUksSUFBYixDQUFrQkMsTUFBbEIsR0FBMkIsSUFBM0I7QUFDSDtBQXZDSSxDQUFUIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJjYy5DbGFzcyh7XG4gICAgZXh0ZW5kczogY2MuQ29tcG9uZW50LFxuXG4gICAgcHJvcGVydGllczoge1xuICAgICAgICB3YXZlVUk6IGNjLk5vZGUsXG4gICAgICAgIGtpbGxEaXNwbGF5OiBjYy5Ob2RlLFxuICAgICAgICBjb21ib0Rpc3BsYXk6IGNjLk5vZGUsXG4gICAgICAgIHBhdXNlVUk6IGNjLk5vZGVcbiAgICAgICAgLy93YXZlUHJvZ3Jlc3M6IGNjLk5vZGVcbiAgICB9LFxuXG4gICAgLy8gdXNlIHRoaXMgZm9yIGluaXRpYWxpemF0aW9uXG4gICAgaW5pdCAoZ2FtZSkge1xuICAgICAgICB0aGlzLndhdmVVSSA9IHRoaXMud2F2ZVVJLmdldENvbXBvbmVudCgnV2F2ZVVJJyk7XG4gICAgICAgIHRoaXMud2F2ZVVJLm5vZGUuYWN0aXZlID0gZmFsc2U7XG4gICAgICAgIHRoaXMua2lsbERpc3BsYXkgPSB0aGlzLmtpbGxEaXNwbGF5LmdldENvbXBvbmVudCgnS2lsbERpc3BsYXknKTtcbiAgICAgICAgdGhpcy5raWxsRGlzcGxheS5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICB0aGlzLmNvbWJvRGlzcGxheSA9IHRoaXMuY29tYm9EaXNwbGF5LmdldENvbXBvbmVudCgnQ29tYm9EaXNwbGF5Jyk7XG4gICAgICAgIHRoaXMuY29tYm9EaXNwbGF5LmluaXQoKTtcbiAgICAgICAgLy8gdGhpcy53YXZlUHJvZ3Jlc3MgPSB0aGlzLndhdmVQcm9ncmVzcy5nZXRDb21wb25lbnQoJ1dhdmVQcm9ncmVzcycpO1xuICAgICAgICAvLyB0aGlzLndhdmVQcm9ncmVzcy5pbml0KGdhbWUud2F2ZU1uZyk7XG4gICAgICAgIFxuICAgIH0sXG5cbiAgICBzaG93V2F2ZSAobnVtKSB7XG4gICAgICAgIHRoaXMud2F2ZVVJLm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICAgICAgdGhpcy53YXZlVUkuc2hvdyhudW0pO1xuICAgIH0sXG4gICAgXG4gICAgc2hvd0tpbGxzIChudW0pIHtcbiAgICAgICAgdGhpcy5raWxsRGlzcGxheS5wbGF5S2lsbChudW0pO1xuICAgIH0sXG4gICAgXG4gICAgYWRkQ29tYm8gKCkge1xuICAgICAgICB0aGlzLmNvbWJvRGlzcGxheS5wbGF5Q29tYm8oKTtcbiAgICB9LFxuICAgXG4gICAgc2hvd1BvcHVwUGF1c2VVSSgpe1xuICAgICAgICB0aGlzLnBhdXNlVUkubm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgIH1cbiAgICBcbn0pO1xuIl19