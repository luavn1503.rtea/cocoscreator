
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/UI/PauseUI.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'c47b0ysWxNBGpK5/fuswtmF', 'PauseUI');
// scripts/UI/PauseUI.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {},
  init: function init(game) {
    this.game = game;
    this.hide();
  },
  Show_popup: function Show_popup() {
    this.node.active = true;
    this.node.opacity = 0;
    this.node.scale = 0.2;
    cc.tween(this.node).to(0.5, {
      scale: 1,
      opacity: 255
    }, {
      easing: "quartInOut"
    }).start();
  },
  Hide: function Hide() {
    var _this = this;

    cc.tween(this.node).to(0.5, {
      scale: 1,
      opacity: 255
    }, {
      easing: "quartInOut"
    }).call(function () {
      _this.node.active = false;
    }).start();
    this.node.active = false;
  },
  "continue": function _continue() {
    this.game["continue"]();
  },
  Quit: function (_Quit) {
    function Quit() {
      return _Quit.apply(this, arguments);
    }

    Quit.toString = function () {
      return _Quit.toString();
    };

    return Quit;
  }(function () {
    Quit.node.active = true;
  }),
  offSound: function (_offSound) {
    function offSound() {
      return _offSound.apply(this, arguments);
    }

    offSound.toString = function () {
      return _offSound.toString();
    };

    return offSound;
  }(function () {
    if (offSound == null) {
      this.node.active = true;
    } else {
      this.node.active = false;
    }
  }),
  open_Tutorial: function (_open_Tutorial) {
    function open_Tutorial() {
      return _open_Tutorial.apply(this, arguments);
    }

    open_Tutorial.toString = function () {
      return _open_Tutorial.toString();
    };

    return open_Tutorial;
  }(function () {
    if (open_Tutorial == null) {
      this.node.active = true;
    } else {
      this.node.active = false;
    }
  })
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcVUlcXFBhdXNlVUkuanMiXSwibmFtZXMiOlsiY2MiLCJDbGFzcyIsIkNvbXBvbmVudCIsInByb3BlcnRpZXMiLCJpbml0IiwiZ2FtZSIsImhpZGUiLCJTaG93X3BvcHVwIiwibm9kZSIsImFjdGl2ZSIsIm9wYWNpdHkiLCJzY2FsZSIsInR3ZWVuIiwidG8iLCJlYXNpbmciLCJzdGFydCIsIkhpZGUiLCJjYWxsIiwiUXVpdCIsIm9mZlNvdW5kIiwib3Blbl9UdXRvcmlhbCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQUEsRUFBRSxDQUFDQyxLQUFILENBQVM7QUFDTCxhQUFTRCxFQUFFLENBQUNFLFNBRFA7QUFHTEMsRUFBQUEsVUFBVSxFQUFFLEVBSFA7QUFNTEMsRUFBQUEsSUFOSyxnQkFNQ0MsSUFORCxFQU1PO0FBQ1IsU0FBS0EsSUFBTCxHQUFZQSxJQUFaO0FBQ0EsU0FBS0MsSUFBTDtBQUNILEdBVEk7QUFXTEMsRUFBQUEsVUFYSyx3QkFXTztBQUNSLFNBQUtDLElBQUwsQ0FBVUMsTUFBVixHQUFtQixJQUFuQjtBQUNBLFNBQUtELElBQUwsQ0FBVUUsT0FBVixHQUFtQixDQUFuQjtBQUNBLFNBQUtGLElBQUwsQ0FBVUcsS0FBVixHQUFpQixHQUFqQjtBQUNBWCxJQUFBQSxFQUFFLENBQUNZLEtBQUgsQ0FBUyxLQUFLSixJQUFkLEVBQW9CSyxFQUFwQixDQUF1QixHQUF2QixFQUEyQjtBQUFDRixNQUFBQSxLQUFLLEVBQUMsQ0FBUDtBQUFTRCxNQUFBQSxPQUFPLEVBQUM7QUFBakIsS0FBM0IsRUFBaUQ7QUFBQ0ksTUFBQUEsTUFBTSxFQUFDO0FBQVIsS0FBakQsRUFBd0VDLEtBQXhFO0FBQ0gsR0FoQkk7QUFrQkxDLEVBQUFBLElBbEJLLGtCQWtCQztBQUFBOztBQUNGaEIsSUFBQUEsRUFBRSxDQUFDWSxLQUFILENBQVMsS0FBS0osSUFBZCxFQUFvQkssRUFBcEIsQ0FBdUIsR0FBdkIsRUFBMkI7QUFBQ0YsTUFBQUEsS0FBSyxFQUFDLENBQVA7QUFBU0QsTUFBQUEsT0FBTyxFQUFDO0FBQWpCLEtBQTNCLEVBQWlEO0FBQUNJLE1BQUFBLE1BQU0sRUFBQztBQUFSLEtBQWpELEVBQ0NHLElBREQsQ0FDTSxZQUFJO0FBQUMsTUFBQSxLQUFJLENBQUNULElBQUwsQ0FBVUMsTUFBVixHQUFtQixLQUFuQjtBQUEwQixLQURyQyxFQUVDTSxLQUZEO0FBR0EsU0FBS1AsSUFBTCxDQUFVQyxNQUFWLEdBQW1CLEtBQW5CO0FBQ0gsR0F2Qkk7QUFBQSxtQ0F5Qks7QUFDTixTQUFLSixJQUFMO0FBQ0gsR0EzQkk7QUE2QkxhLEVBQUFBLElBN0JLO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBLGdCQTZCQztBQUNGQSxJQUFBQSxJQUFJLENBQUNWLElBQUwsQ0FBVUMsTUFBVixHQUFtQixJQUFuQjtBQUVILEdBaENJO0FBa0NMVSxFQUFBQSxRQWxDSztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQSxnQkFtQ0w7QUFDRyxRQUFHQSxRQUFRLElBQUcsSUFBZCxFQUNBO0FBQ0ksV0FBS1gsSUFBTCxDQUFVQyxNQUFWLEdBQW1CLElBQW5CO0FBQ0gsS0FIRCxNQUtBO0FBQ0ksV0FBS0QsSUFBTCxDQUFVQyxNQUFWLEdBQW1CLEtBQW5CO0FBQ0g7QUFFSCxHQTdDSTtBQThDTFcsRUFBQUEsYUE5Q0s7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUEsZ0JBK0NMO0FBQ0ksUUFBR0EsYUFBYSxJQUFHLElBQW5CLEVBQ0E7QUFDSSxXQUFLWixJQUFMLENBQVVDLE1BQVYsR0FBbUIsSUFBbkI7QUFDSCxLQUhELE1BS0E7QUFDSSxXQUFLRCxJQUFMLENBQVVDLE1BQVYsR0FBbUIsS0FBbkI7QUFDSDtBQUNKLEdBeERJO0FBQUEsQ0FBVCIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbmNjLkNsYXNzKHtcclxuICAgIGV4dGVuZHM6IGNjLkNvbXBvbmVudCxcclxuXHJcbiAgICBwcm9wZXJ0aWVzOiB7XHJcbiAgICAgIFxyXG4gICAgfSxcclxuICAgIGluaXQgKGdhbWUpIHtcclxuICAgICAgICB0aGlzLmdhbWUgPSBnYW1lO1xyXG4gICAgICAgIHRoaXMuaGlkZSgpO1xyXG4gICAgfSxcclxuXHJcbiAgICBTaG93X3BvcHVwKCl7XHJcbiAgICAgICAgdGhpcy5ub2RlLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5ub2RlLm9wYWNpdHkgPTA7XHJcbiAgICAgICAgdGhpcy5ub2RlLnNjYWxlID0wLjI7XHJcbiAgICAgICAgY2MudHdlZW4odGhpcy5ub2RlKS50bygwLjUse3NjYWxlOjEsb3BhY2l0eToyNTV9LHtlYXNpbmc6XCJxdWFydEluT3V0XCJ9KS5zdGFydCgpO1xyXG4gICAgfSxcclxuXHJcbiAgICBIaWRlKCl7XHJcbiAgICAgICAgY2MudHdlZW4odGhpcy5ub2RlKS50bygwLjUse3NjYWxlOjEsb3BhY2l0eToyNTV9LHtlYXNpbmc6XCJxdWFydEluT3V0XCJ9KVxyXG4gICAgICAgIC5jYWxsKCgpPT57dGhpcy5ub2RlLmFjdGl2ZSA9IGZhbHNlO30pXHJcbiAgICAgICAgLnN0YXJ0KCk7XHJcbiAgICAgICAgdGhpcy5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgfSxcclxuXHJcbiAgICBjb250aW51ZSgpe1xyXG4gICAgICAgIHRoaXMuZ2FtZS5jb250aW51ZSgpO1xyXG4gICAgfSxcclxuXHJcbiAgICBRdWl0KCl7XHJcbiAgICAgICAgUXVpdC5ub2RlLmFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgXHJcbiAgICB9LFxyXG5cclxuICAgIG9mZlNvdW5kKClcclxuICAgIHtcclxuICAgICAgIGlmKG9mZlNvdW5kPT0gbnVsbClcclxuICAgICAgIHtcclxuICAgICAgICAgICB0aGlzLm5vZGUuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgIH1cclxuICAgICAgIGVsc2VcclxuICAgICAgIHtcclxuICAgICAgICAgICB0aGlzLm5vZGUuYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICB9LFxyXG4gICAgb3Blbl9UdXRvcmlhbCgpXHJcbiAgICB7XHJcbiAgICAgICAgaWYob3Blbl9UdXRvcmlhbD09IG51bGwpXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICB0aGlzLm5vZGUuYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgdGhpcy5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbn0pO1xyXG4iXX0=