
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/NodePool.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '4762cqE38NHn451+NhWnQ5U', 'NodePool');
// scripts/NodePool.js

"use strict";

var NodePool = cc.Class({
  name: 'NodePool',
  properties: {
    prefab: cc.Prefab,
    size: 0
  },
  ctor: function ctor() {
    this.idx = 0;
    this.initList = [];
    this.list = [];
  },
  init: function init() {
    for (var i = 0; i < this.size; ++i) {
      var obj = cc.instantiate(this.prefab);
      this.initList[i] = obj;
      this.list[i] = obj;
    }

    this.idx = this.size - 1;
  },
  reset: function reset() {
    for (var i = 0; i < this.size; ++i) {
      var obj = this.initList[i];
      this.list[i] = obj;

      if (obj.active) {
        obj.active = false;
      }

      if (obj.parent) {
        obj.removeFromParent();
      }
    }

    this.idx = this.size - 1;
  },
  request: function request() {
    if (this.idx < 0) {
      cc.log("Error: the pool do not have enough free item.");
      return null;
    }

    var obj = this.list[this.idx];

    if (obj) {
      obj.active = true;
    }

    --this.idx;
    return obj;
  },
  "return": function _return(obj) {
    ++this.idx;
    obj.active = false;

    if (obj.parent) {
      obj.removeFromParent();
    }

    this.list[this.idx] = obj;
  }
});
module.exports = NodePool;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcTm9kZVBvb2wuanMiXSwibmFtZXMiOlsiTm9kZVBvb2wiLCJjYyIsIkNsYXNzIiwibmFtZSIsInByb3BlcnRpZXMiLCJwcmVmYWIiLCJQcmVmYWIiLCJzaXplIiwiY3RvciIsImlkeCIsImluaXRMaXN0IiwibGlzdCIsImluaXQiLCJpIiwib2JqIiwiaW5zdGFudGlhdGUiLCJyZXNldCIsImFjdGl2ZSIsInBhcmVudCIsInJlbW92ZUZyb21QYXJlbnQiLCJyZXF1ZXN0IiwibG9nIiwibW9kdWxlIiwiZXhwb3J0cyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxJQUFJQSxRQUFRLEdBQUdDLEVBQUUsQ0FBQ0MsS0FBSCxDQUFTO0FBQ3BCQyxFQUFBQSxJQUFJLEVBQUUsVUFEYztBQUVwQkMsRUFBQUEsVUFBVSxFQUFFO0FBQ1JDLElBQUFBLE1BQU0sRUFBRUosRUFBRSxDQUFDSyxNQURIO0FBRVJDLElBQUFBLElBQUksRUFBRTtBQUZFLEdBRlE7QUFNcEJDLEVBQUFBLElBTm9CLGtCQU1aO0FBQ0osU0FBS0MsR0FBTCxHQUFXLENBQVg7QUFDQSxTQUFLQyxRQUFMLEdBQWdCLEVBQWhCO0FBQ0EsU0FBS0MsSUFBTCxHQUFZLEVBQVo7QUFDSCxHQVZtQjtBQVdwQkMsRUFBQUEsSUFYb0Isa0JBV1o7QUFDSixTQUFNLElBQUlDLENBQUMsR0FBRyxDQUFkLEVBQWlCQSxDQUFDLEdBQUcsS0FBS04sSUFBMUIsRUFBZ0MsRUFBRU0sQ0FBbEMsRUFBc0M7QUFDbEMsVUFBSUMsR0FBRyxHQUFHYixFQUFFLENBQUNjLFdBQUgsQ0FBZSxLQUFLVixNQUFwQixDQUFWO0FBQ0EsV0FBS0ssUUFBTCxDQUFjRyxDQUFkLElBQW1CQyxHQUFuQjtBQUNBLFdBQUtILElBQUwsQ0FBVUUsQ0FBVixJQUFlQyxHQUFmO0FBQ0g7O0FBQ0QsU0FBS0wsR0FBTCxHQUFXLEtBQUtGLElBQUwsR0FBWSxDQUF2QjtBQUNILEdBbEJtQjtBQW1CcEJTLEVBQUFBLEtBbkJvQixtQkFtQlg7QUFDTCxTQUFNLElBQUlILENBQUMsR0FBRyxDQUFkLEVBQWlCQSxDQUFDLEdBQUcsS0FBS04sSUFBMUIsRUFBZ0MsRUFBRU0sQ0FBbEMsRUFBc0M7QUFDbEMsVUFBSUMsR0FBRyxHQUFHLEtBQUtKLFFBQUwsQ0FBY0csQ0FBZCxDQUFWO0FBQ0EsV0FBS0YsSUFBTCxDQUFVRSxDQUFWLElBQWVDLEdBQWY7O0FBQ0EsVUFBSUEsR0FBRyxDQUFDRyxNQUFSLEVBQWdCO0FBQ1pILFFBQUFBLEdBQUcsQ0FBQ0csTUFBSixHQUFhLEtBQWI7QUFDSDs7QUFDRCxVQUFJSCxHQUFHLENBQUNJLE1BQVIsRUFBZ0I7QUFDWkosUUFBQUEsR0FBRyxDQUFDSyxnQkFBSjtBQUNIO0FBQ0o7O0FBQ0QsU0FBS1YsR0FBTCxHQUFXLEtBQUtGLElBQUwsR0FBWSxDQUF2QjtBQUNILEdBL0JtQjtBQWlDcEJhLEVBQUFBLE9BakNvQixxQkFpQ1I7QUFDUixRQUFLLEtBQUtYLEdBQUwsR0FBVyxDQUFoQixFQUFvQjtBQUNoQlIsTUFBQUEsRUFBRSxDQUFDb0IsR0FBSCxDQUFRLCtDQUFSO0FBQ0EsYUFBTyxJQUFQO0FBQ0g7O0FBQ0QsUUFBSVAsR0FBRyxHQUFHLEtBQUtILElBQUwsQ0FBVSxLQUFLRixHQUFmLENBQVY7O0FBQ0EsUUFBS0ssR0FBTCxFQUFXO0FBQ1BBLE1BQUFBLEdBQUcsQ0FBQ0csTUFBSixHQUFhLElBQWI7QUFDSDs7QUFDRCxNQUFFLEtBQUtSLEdBQVA7QUFDQSxXQUFPSyxHQUFQO0FBQ0gsR0E1Q21CO0FBQUEsNkJBNkNYQSxHQTdDVyxFQTZDTDtBQUNYLE1BQUUsS0FBS0wsR0FBUDtBQUNBSyxJQUFBQSxHQUFHLENBQUNHLE1BQUosR0FBYSxLQUFiOztBQUNBLFFBQUlILEdBQUcsQ0FBQ0ksTUFBUixFQUFnQjtBQUNaSixNQUFBQSxHQUFHLENBQUNLLGdCQUFKO0FBQ0g7O0FBQ0QsU0FBS1IsSUFBTCxDQUFVLEtBQUtGLEdBQWYsSUFBc0JLLEdBQXRCO0FBQ0g7QUFwRG1CLENBQVQsQ0FBZjtBQXVEQVEsTUFBTSxDQUFDQyxPQUFQLEdBQWlCdkIsUUFBakIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbInZhciBOb2RlUG9vbCA9IGNjLkNsYXNzKHtcbiAgICBuYW1lOiAnTm9kZVBvb2wnLFxuICAgIHByb3BlcnRpZXM6IHtcbiAgICAgICAgcHJlZmFiOiBjYy5QcmVmYWIsXG4gICAgICAgIHNpemU6IDBcbiAgICB9LFxuICAgIGN0b3IgKCkge1xuICAgICAgICB0aGlzLmlkeCA9IDA7XG4gICAgICAgIHRoaXMuaW5pdExpc3QgPSBbXTtcbiAgICAgICAgdGhpcy5saXN0ID0gW107XG4gICAgfSxcbiAgICBpbml0ICgpIHtcbiAgICAgICAgZm9yICggbGV0IGkgPSAwOyBpIDwgdGhpcy5zaXplOyArK2kgKSB7XG4gICAgICAgICAgICBsZXQgb2JqID0gY2MuaW5zdGFudGlhdGUodGhpcy5wcmVmYWIpO1xuICAgICAgICAgICAgdGhpcy5pbml0TGlzdFtpXSA9IG9iajtcbiAgICAgICAgICAgIHRoaXMubGlzdFtpXSA9IG9iajtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLmlkeCA9IHRoaXMuc2l6ZSAtIDE7XG4gICAgfSxcbiAgICByZXNldCAoKSB7XG4gICAgICAgIGZvciAoIGxldCBpID0gMDsgaSA8IHRoaXMuc2l6ZTsgKytpICkge1xuICAgICAgICAgICAgbGV0IG9iaiA9IHRoaXMuaW5pdExpc3RbaV07XG4gICAgICAgICAgICB0aGlzLmxpc3RbaV0gPSBvYmo7XG4gICAgICAgICAgICBpZiAob2JqLmFjdGl2ZSkge1xuICAgICAgICAgICAgICAgIG9iai5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChvYmoucGFyZW50KSB7XG4gICAgICAgICAgICAgICAgb2JqLnJlbW92ZUZyb21QYXJlbnQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICB0aGlzLmlkeCA9IHRoaXMuc2l6ZSAtIDE7XG4gICAgfSxcblxuICAgIHJlcXVlc3QgKCkgIHtcbiAgICAgICAgaWYgKCB0aGlzLmlkeCA8IDAgKSB7XG4gICAgICAgICAgICBjYy5sb2cgKFwiRXJyb3I6IHRoZSBwb29sIGRvIG5vdCBoYXZlIGVub3VnaCBmcmVlIGl0ZW0uXCIpO1xuICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgIH1cbiAgICAgICAgbGV0IG9iaiA9IHRoaXMubGlzdFt0aGlzLmlkeF07XG4gICAgICAgIGlmICggb2JqICkge1xuICAgICAgICAgICAgb2JqLmFjdGl2ZSA9IHRydWU7XG4gICAgICAgIH1cbiAgICAgICAgLS10aGlzLmlkeDtcbiAgICAgICAgcmV0dXJuIG9iajtcbiAgICB9LFxuICAgIHJldHVybiAoIG9iaiApIHtcbiAgICAgICAgKyt0aGlzLmlkeDtcbiAgICAgICAgb2JqLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICBpZiAob2JqLnBhcmVudCkge1xuICAgICAgICAgICAgb2JqLnJlbW92ZUZyb21QYXJlbnQoKTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLmxpc3RbdGhpcy5pZHhdID0gb2JqO1xuICAgIH1cbn0pO1xuXG5tb2R1bGUuZXhwb3J0cyA9IE5vZGVQb29sOyJdfQ==