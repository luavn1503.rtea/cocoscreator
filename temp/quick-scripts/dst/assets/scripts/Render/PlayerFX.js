
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/Render/PlayerFX.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '473ddOy0BlLraG4rFvYcoyb', 'PlayerFX');
// scripts/Render/PlayerFX.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {
    introAnim: cc.Animation,
    reviveAnim: cc.Animation
  },
  // use this for initialization
  init: function init(game) {
    this.game = game;
    this.introAnim.node.active = false;
    this.reviveAnim.node.active = false;
  },
  playIntro: function playIntro() {
    this.introAnim.node.active = true;
    this.introAnim.play('start');
  },
  playRevive: function playRevive() {
    this.reviveAnim.node.active = true;
    this.reviveAnim.node.setPosition(this.game.player.node.position);
    this.reviveAnim.play('revive');
  },
  introFinish: function introFinish() {
    this.game.playerReady();
    this.introAnim.node.active = false;
  },
  reviveFinish: function reviveFinish() {
    this.game.playerReady();
    this.reviveAnim.node.active = false;
  },
  reviveKill: function reviveKill() {
    // kill all enemies
    this.game.clearAllFoes();
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcUmVuZGVyXFxQbGF5ZXJGWC5qcyJdLCJuYW1lcyI6WyJjYyIsIkNsYXNzIiwiQ29tcG9uZW50IiwicHJvcGVydGllcyIsImludHJvQW5pbSIsIkFuaW1hdGlvbiIsInJldml2ZUFuaW0iLCJpbml0IiwiZ2FtZSIsIm5vZGUiLCJhY3RpdmUiLCJwbGF5SW50cm8iLCJwbGF5IiwicGxheVJldml2ZSIsInNldFBvc2l0aW9uIiwicGxheWVyIiwicG9zaXRpb24iLCJpbnRyb0ZpbmlzaCIsInBsYXllclJlYWR5IiwicmV2aXZlRmluaXNoIiwicmV2aXZlS2lsbCIsImNsZWFyQWxsRm9lcyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQUEsRUFBRSxDQUFDQyxLQUFILENBQVM7QUFDTCxhQUFTRCxFQUFFLENBQUNFLFNBRFA7QUFHTEMsRUFBQUEsVUFBVSxFQUFFO0FBQ1JDLElBQUFBLFNBQVMsRUFBRUosRUFBRSxDQUFDSyxTQUROO0FBRVJDLElBQUFBLFVBQVUsRUFBRU4sRUFBRSxDQUFDSztBQUZQLEdBSFA7QUFRTDtBQUNBRSxFQUFBQSxJQVRLLGdCQVNDQyxJQVRELEVBU087QUFDUixTQUFLQSxJQUFMLEdBQVlBLElBQVo7QUFDQSxTQUFLSixTQUFMLENBQWVLLElBQWYsQ0FBb0JDLE1BQXBCLEdBQTZCLEtBQTdCO0FBQ0EsU0FBS0osVUFBTCxDQUFnQkcsSUFBaEIsQ0FBcUJDLE1BQXJCLEdBQThCLEtBQTlCO0FBQ0gsR0FiSTtBQWVMQyxFQUFBQSxTQWZLLHVCQWVRO0FBQ1QsU0FBS1AsU0FBTCxDQUFlSyxJQUFmLENBQW9CQyxNQUFwQixHQUE2QixJQUE3QjtBQUNBLFNBQUtOLFNBQUwsQ0FBZVEsSUFBZixDQUFvQixPQUFwQjtBQUNILEdBbEJJO0FBb0JMQyxFQUFBQSxVQXBCSyx3QkFvQlM7QUFDVixTQUFLUCxVQUFMLENBQWdCRyxJQUFoQixDQUFxQkMsTUFBckIsR0FBOEIsSUFBOUI7QUFDQSxTQUFLSixVQUFMLENBQWdCRyxJQUFoQixDQUFxQkssV0FBckIsQ0FBaUMsS0FBS04sSUFBTCxDQUFVTyxNQUFWLENBQWlCTixJQUFqQixDQUFzQk8sUUFBdkQ7QUFDQSxTQUFLVixVQUFMLENBQWdCTSxJQUFoQixDQUFxQixRQUFyQjtBQUNILEdBeEJJO0FBMEJMSyxFQUFBQSxXQTFCSyx5QkEwQlU7QUFDWCxTQUFLVCxJQUFMLENBQVVVLFdBQVY7QUFDQSxTQUFLZCxTQUFMLENBQWVLLElBQWYsQ0FBb0JDLE1BQXBCLEdBQTZCLEtBQTdCO0FBQ0gsR0E3Qkk7QUErQkxTLEVBQUFBLFlBL0JLLDBCQStCVztBQUNaLFNBQUtYLElBQUwsQ0FBVVUsV0FBVjtBQUNBLFNBQUtaLFVBQUwsQ0FBZ0JHLElBQWhCLENBQXFCQyxNQUFyQixHQUE4QixLQUE5QjtBQUNILEdBbENJO0FBb0NMVSxFQUFBQSxVQXBDSyx3QkFvQ1M7QUFBRTtBQUNaLFNBQUtaLElBQUwsQ0FBVWEsWUFBVjtBQUNIO0FBdENJLENBQVQiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImNjLkNsYXNzKHtcbiAgICBleHRlbmRzOiBjYy5Db21wb25lbnQsXG5cbiAgICBwcm9wZXJ0aWVzOiB7XG4gICAgICAgIGludHJvQW5pbTogY2MuQW5pbWF0aW9uLFxuICAgICAgICByZXZpdmVBbmltOiBjYy5BbmltYXRpb25cbiAgICB9LFxuXG4gICAgLy8gdXNlIHRoaXMgZm9yIGluaXRpYWxpemF0aW9uXG4gICAgaW5pdCAoZ2FtZSkge1xuICAgICAgICB0aGlzLmdhbWUgPSBnYW1lO1xuICAgICAgICB0aGlzLmludHJvQW5pbS5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICB0aGlzLnJldml2ZUFuaW0ubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICB9LFxuXG4gICAgcGxheUludHJvICgpIHtcbiAgICAgICAgdGhpcy5pbnRyb0FuaW0ubm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgICAgICB0aGlzLmludHJvQW5pbS5wbGF5KCdzdGFydCcpO1xuICAgIH0sXG5cbiAgICBwbGF5UmV2aXZlICgpIHtcbiAgICAgICAgdGhpcy5yZXZpdmVBbmltLm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5yZXZpdmVBbmltLm5vZGUuc2V0UG9zaXRpb24odGhpcy5nYW1lLnBsYXllci5ub2RlLnBvc2l0aW9uKTtcbiAgICAgICAgdGhpcy5yZXZpdmVBbmltLnBsYXkoJ3Jldml2ZScpO1xuICAgIH0sXG5cbiAgICBpbnRyb0ZpbmlzaCAoKSB7XG4gICAgICAgIHRoaXMuZ2FtZS5wbGF5ZXJSZWFkeSgpO1xuICAgICAgICB0aGlzLmludHJvQW5pbS5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgIH0sXG5cbiAgICByZXZpdmVGaW5pc2ggKCkge1xuICAgICAgICB0aGlzLmdhbWUucGxheWVyUmVhZHkoKTtcbiAgICAgICAgdGhpcy5yZXZpdmVBbmltLm5vZGUuYWN0aXZlID0gZmFsc2U7XG4gICAgfSxcbiAgICBcbiAgICByZXZpdmVLaWxsICgpIHsgLy8ga2lsbCBhbGwgZW5lbWllc1xuICAgICAgICB0aGlzLmdhbWUuY2xlYXJBbGxGb2VzKCk7XG4gICAgfVxufSk7XG4iXX0=