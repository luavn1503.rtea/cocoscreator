
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/Render/ComboDisplay.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '8eb3c3ywppFI6IWH3Np7yc9', 'ComboDisplay');
// scripts/Render/ComboDisplay.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {
    labelCombo: cc.Label,
    spFlare: cc.Sprite,
    anim: cc.Animation,
    comboColors: [cc.Color],
    showDuration: 0
  },
  init: function init() {
    this.comboCount = 0;
    this.node.active = false;
    this.showTimer = 0;
  },
  // use this for initialization
  playCombo: function playCombo() {
    this.comboCount++;
    this.node.active = true; // this.unschedule(this.hide);

    var colorIdx = Math.min(Math.floor(this.comboCount / 10), this.comboColors.length - 1);
    this.spFlare.node.color = this.comboColors[colorIdx];
    this.labelCombo.node.color = this.comboColors[colorIdx];
    this.labelCombo.string = this.comboCount;
    this.anim.play('combo-pop');
    this.showTimer = 0; // this.scheduleOnce(this.hide.bind(this), this.showDuration );
  },
  // called every frame, uncomment this function to activate update callback
  hide: function hide() {
    this.comboCount = 0;
    this.node.active = false;
  },
  update: function update(dt) {
    if (!this.node.active) {
      return;
    }

    this.showTimer += dt;

    if (this.showTimer >= this.showDuration) {
      this.hide();
    }
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcUmVuZGVyXFxDb21ib0Rpc3BsYXkuanMiXSwibmFtZXMiOlsiY2MiLCJDbGFzcyIsIkNvbXBvbmVudCIsInByb3BlcnRpZXMiLCJsYWJlbENvbWJvIiwiTGFiZWwiLCJzcEZsYXJlIiwiU3ByaXRlIiwiYW5pbSIsIkFuaW1hdGlvbiIsImNvbWJvQ29sb3JzIiwiQ29sb3IiLCJzaG93RHVyYXRpb24iLCJpbml0IiwiY29tYm9Db3VudCIsIm5vZGUiLCJhY3RpdmUiLCJzaG93VGltZXIiLCJwbGF5Q29tYm8iLCJjb2xvcklkeCIsIk1hdGgiLCJtaW4iLCJmbG9vciIsImxlbmd0aCIsImNvbG9yIiwic3RyaW5nIiwicGxheSIsImhpZGUiLCJ1cGRhdGUiLCJkdCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQUEsRUFBRSxDQUFDQyxLQUFILENBQVM7QUFDTCxhQUFTRCxFQUFFLENBQUNFLFNBRFA7QUFHTEMsRUFBQUEsVUFBVSxFQUFFO0FBQ1JDLElBQUFBLFVBQVUsRUFBRUosRUFBRSxDQUFDSyxLQURQO0FBRVJDLElBQUFBLE9BQU8sRUFBRU4sRUFBRSxDQUFDTyxNQUZKO0FBR1JDLElBQUFBLElBQUksRUFBRVIsRUFBRSxDQUFDUyxTQUhEO0FBSVJDLElBQUFBLFdBQVcsRUFBRSxDQUFDVixFQUFFLENBQUNXLEtBQUosQ0FKTDtBQUtSQyxJQUFBQSxZQUFZLEVBQUU7QUFMTixHQUhQO0FBV0xDLEVBQUFBLElBWEssa0JBV0c7QUFDSixTQUFLQyxVQUFMLEdBQWtCLENBQWxCO0FBQ0EsU0FBS0MsSUFBTCxDQUFVQyxNQUFWLEdBQW1CLEtBQW5CO0FBQ0EsU0FBS0MsU0FBTCxHQUFpQixDQUFqQjtBQUNILEdBZkk7QUFpQkw7QUFDQUMsRUFBQUEsU0FsQkssdUJBa0JRO0FBQ1QsU0FBS0osVUFBTDtBQUNBLFNBQUtDLElBQUwsQ0FBVUMsTUFBVixHQUFtQixJQUFuQixDQUZTLENBR1Q7O0FBQ0EsUUFBSUcsUUFBUSxHQUFHQyxJQUFJLENBQUNDLEdBQUwsQ0FBU0QsSUFBSSxDQUFDRSxLQUFMLENBQVcsS0FBS1IsVUFBTCxHQUFrQixFQUE3QixDQUFULEVBQTJDLEtBQUtKLFdBQUwsQ0FBaUJhLE1BQWpCLEdBQTBCLENBQXJFLENBQWY7QUFDQSxTQUFLakIsT0FBTCxDQUFhUyxJQUFiLENBQWtCUyxLQUFsQixHQUEwQixLQUFLZCxXQUFMLENBQWlCUyxRQUFqQixDQUExQjtBQUNBLFNBQUtmLFVBQUwsQ0FBZ0JXLElBQWhCLENBQXFCUyxLQUFyQixHQUE2QixLQUFLZCxXQUFMLENBQWlCUyxRQUFqQixDQUE3QjtBQUNBLFNBQUtmLFVBQUwsQ0FBZ0JxQixNQUFoQixHQUF5QixLQUFLWCxVQUE5QjtBQUNBLFNBQUtOLElBQUwsQ0FBVWtCLElBQVYsQ0FBZSxXQUFmO0FBQ0EsU0FBS1QsU0FBTCxHQUFpQixDQUFqQixDQVRTLENBVVQ7QUFDSCxHQTdCSTtBQStCTDtBQUNBVSxFQUFBQSxJQWhDSyxrQkFnQ0c7QUFDSixTQUFLYixVQUFMLEdBQWtCLENBQWxCO0FBQ0EsU0FBS0MsSUFBTCxDQUFVQyxNQUFWLEdBQW1CLEtBQW5CO0FBQ0gsR0FuQ0k7QUFxQ0xZLEVBQUFBLE1BckNLLGtCQXFDR0MsRUFyQ0gsRUFxQ087QUFDUixRQUFJLENBQUMsS0FBS2QsSUFBTCxDQUFVQyxNQUFmLEVBQXVCO0FBQ25CO0FBQ0g7O0FBRUQsU0FBS0MsU0FBTCxJQUFrQlksRUFBbEI7O0FBQ0EsUUFBSSxLQUFLWixTQUFMLElBQWtCLEtBQUtMLFlBQTNCLEVBQXlDO0FBQ3JDLFdBQUtlLElBQUw7QUFDSDtBQUNKO0FBOUNJLENBQVQiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImNjLkNsYXNzKHtcbiAgICBleHRlbmRzOiBjYy5Db21wb25lbnQsXG5cbiAgICBwcm9wZXJ0aWVzOiB7XG4gICAgICAgIGxhYmVsQ29tYm86IGNjLkxhYmVsLFxuICAgICAgICBzcEZsYXJlOiBjYy5TcHJpdGUsXG4gICAgICAgIGFuaW06IGNjLkFuaW1hdGlvbixcbiAgICAgICAgY29tYm9Db2xvcnM6IFtjYy5Db2xvcl0sXG4gICAgICAgIHNob3dEdXJhdGlvbjogMFxuICAgIH0sXG4gICAgXG4gICAgaW5pdCAoKSB7XG4gICAgICAgIHRoaXMuY29tYm9Db3VudCA9IDA7XG4gICAgICAgIHRoaXMubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5zaG93VGltZXIgPSAwO1xuICAgIH0sXG5cbiAgICAvLyB1c2UgdGhpcyBmb3IgaW5pdGlhbGl6YXRpb25cbiAgICBwbGF5Q29tYm8gKCkge1xuICAgICAgICB0aGlzLmNvbWJvQ291bnQrKztcbiAgICAgICAgdGhpcy5ub2RlLmFjdGl2ZSA9IHRydWU7XG4gICAgICAgIC8vIHRoaXMudW5zY2hlZHVsZSh0aGlzLmhpZGUpO1xuICAgICAgICBsZXQgY29sb3JJZHggPSBNYXRoLm1pbihNYXRoLmZsb29yKHRoaXMuY29tYm9Db3VudCAvIDEwKSwgdGhpcy5jb21ib0NvbG9ycy5sZW5ndGggLSAxKTtcbiAgICAgICAgdGhpcy5zcEZsYXJlLm5vZGUuY29sb3IgPSB0aGlzLmNvbWJvQ29sb3JzW2NvbG9ySWR4XTtcbiAgICAgICAgdGhpcy5sYWJlbENvbWJvLm5vZGUuY29sb3IgPSB0aGlzLmNvbWJvQ29sb3JzW2NvbG9ySWR4XTtcbiAgICAgICAgdGhpcy5sYWJlbENvbWJvLnN0cmluZyA9IHRoaXMuY29tYm9Db3VudDtcbiAgICAgICAgdGhpcy5hbmltLnBsYXkoJ2NvbWJvLXBvcCcpO1xuICAgICAgICB0aGlzLnNob3dUaW1lciA9IDA7XG4gICAgICAgIC8vIHRoaXMuc2NoZWR1bGVPbmNlKHRoaXMuaGlkZS5iaW5kKHRoaXMpLCB0aGlzLnNob3dEdXJhdGlvbiApO1xuICAgIH0sXG5cbiAgICAvLyBjYWxsZWQgZXZlcnkgZnJhbWUsIHVuY29tbWVudCB0aGlzIGZ1bmN0aW9uIHRvIGFjdGl2YXRlIHVwZGF0ZSBjYWxsYmFja1xuICAgIGhpZGUgKCkge1xuICAgICAgICB0aGlzLmNvbWJvQ291bnQgPSAwO1xuICAgICAgICB0aGlzLm5vZGUuYWN0aXZlID0gZmFsc2U7XG4gICAgfSxcblxuICAgIHVwZGF0ZSAoZHQpIHtcbiAgICAgICAgaWYgKCF0aGlzLm5vZGUuYWN0aXZlKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLnNob3dUaW1lciArPSBkdDtcbiAgICAgICAgaWYgKHRoaXMuc2hvd1RpbWVyID49IHRoaXMuc2hvd0R1cmF0aW9uKSB7XG4gICAgICAgICAgICB0aGlzLmhpZGUoKTtcbiAgICAgICAgfVxuICAgIH1cbn0pO1xuIl19