
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/Render/SortMng.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '61165e1leJB+4dbkFmx1myT', 'SortMng');
// scripts/Render/SortMng.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {},
  // use this for initialization
  init: function init() {
    this.frameCount = 0;
  },
  // called every frame, uncomment this function to activate update callback
  update: function update(dt) {
    if (++this.frameCount % 6 === 0) {
      this.sortChildrenByY();
    }
  },
  sortChildrenByY: function sortChildrenByY() {
    var listToSort = this.node.children.slice();
    listToSort.sort(function (a, b) {
      return b.y - a.y;
    });

    for (var i = 0; i < listToSort.length; ++i) {
      var node = listToSort[i];

      if (node.active) {
        node.setSiblingIndex(i);
      }
    }
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcUmVuZGVyXFxTb3J0TW5nLmpzIl0sIm5hbWVzIjpbImNjIiwiQ2xhc3MiLCJDb21wb25lbnQiLCJwcm9wZXJ0aWVzIiwiaW5pdCIsImZyYW1lQ291bnQiLCJ1cGRhdGUiLCJkdCIsInNvcnRDaGlsZHJlbkJ5WSIsImxpc3RUb1NvcnQiLCJub2RlIiwiY2hpbGRyZW4iLCJzbGljZSIsInNvcnQiLCJhIiwiYiIsInkiLCJpIiwibGVuZ3RoIiwiYWN0aXZlIiwic2V0U2libGluZ0luZGV4Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBQSxFQUFFLENBQUNDLEtBQUgsQ0FBUztBQUNMLGFBQVNELEVBQUUsQ0FBQ0UsU0FEUDtBQUdMQyxFQUFBQSxVQUFVLEVBQUUsRUFIUDtBQU1MO0FBQ0FDLEVBQUFBLElBQUksRUFBRSxnQkFBWTtBQUNkLFNBQUtDLFVBQUwsR0FBa0IsQ0FBbEI7QUFDSCxHQVRJO0FBV0w7QUFDQUMsRUFBQUEsTUFBTSxFQUFFLGdCQUFVQyxFQUFWLEVBQWM7QUFDbEIsUUFBSSxFQUFFLEtBQUtGLFVBQVAsR0FBb0IsQ0FBcEIsS0FBMEIsQ0FBOUIsRUFBaUM7QUFDN0IsV0FBS0csZUFBTDtBQUNIO0FBQ0osR0FoQkk7QUFrQkxBLEVBQUFBLGVBbEJLLDZCQWtCYztBQUNmLFFBQUlDLFVBQVUsR0FBRyxLQUFLQyxJQUFMLENBQVVDLFFBQVYsQ0FBbUJDLEtBQW5CLEVBQWpCO0FBQ0FILElBQUFBLFVBQVUsQ0FBQ0ksSUFBWCxDQUFnQixVQUFVQyxDQUFWLEVBQWFDLENBQWIsRUFBZTtBQUMzQixhQUFPQSxDQUFDLENBQUNDLENBQUYsR0FBTUYsQ0FBQyxDQUFDRSxDQUFmO0FBQ0gsS0FGRDs7QUFHQSxTQUFLLElBQUlDLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdSLFVBQVUsQ0FBQ1MsTUFBL0IsRUFBdUMsRUFBRUQsQ0FBekMsRUFBNEM7QUFDeEMsVUFBSVAsSUFBSSxHQUFHRCxVQUFVLENBQUNRLENBQUQsQ0FBckI7O0FBQ0EsVUFBSVAsSUFBSSxDQUFDUyxNQUFULEVBQWlCO0FBQ2JULFFBQUFBLElBQUksQ0FBQ1UsZUFBTCxDQUFxQkgsQ0FBckI7QUFDSDtBQUNKO0FBQ0o7QUE3QkksQ0FBVCIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiY2MuQ2xhc3Moe1xuICAgIGV4dGVuZHM6IGNjLkNvbXBvbmVudCxcblxuICAgIHByb3BlcnRpZXM6IHtcbiAgICB9LFxuXG4gICAgLy8gdXNlIHRoaXMgZm9yIGluaXRpYWxpemF0aW9uXG4gICAgaW5pdDogZnVuY3Rpb24gKCkge1xuICAgICAgICB0aGlzLmZyYW1lQ291bnQgPSAwO1xuICAgIH0sXG5cbiAgICAvLyBjYWxsZWQgZXZlcnkgZnJhbWUsIHVuY29tbWVudCB0aGlzIGZ1bmN0aW9uIHRvIGFjdGl2YXRlIHVwZGF0ZSBjYWxsYmFja1xuICAgIHVwZGF0ZTogZnVuY3Rpb24gKGR0KSB7XG4gICAgICAgIGlmICgrK3RoaXMuZnJhbWVDb3VudCAlIDYgPT09IDApIHtcbiAgICAgICAgICAgIHRoaXMuc29ydENoaWxkcmVuQnlZKCk7XG4gICAgICAgIH1cbiAgICB9LFxuICAgIFxuICAgIHNvcnRDaGlsZHJlbkJ5WSAoKSB7XG4gICAgICAgIGxldCBsaXN0VG9Tb3J0ID0gdGhpcy5ub2RlLmNoaWxkcmVuLnNsaWNlKCk7XG4gICAgICAgIGxpc3RUb1NvcnQuc29ydChmdW5jdGlvbiAoYSwgYil7XG4gICAgICAgICAgICByZXR1cm4gYi55IC0gYS55O1xuICAgICAgICB9KTtcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBsaXN0VG9Tb3J0Lmxlbmd0aDsgKytpKSB7XG4gICAgICAgICAgICBsZXQgbm9kZSA9IGxpc3RUb1NvcnRbaV07XG4gICAgICAgICAgICBpZiAobm9kZS5hY3RpdmUpIHtcbiAgICAgICAgICAgICAgICBub2RlLnNldFNpYmxpbmdJbmRleChpKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn0pO1xuIl19