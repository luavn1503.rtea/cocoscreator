
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/Render/KillDisplay.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '592dd/u1/tEOJgCdwAZWLVG', 'KillDisplay');
// scripts/Render/KillDisplay.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {
    labelKills: cc.Label,
    anim: cc.Animation
  },
  playKill: function playKill(kills) {
    this.node.active = true;
    this.labelKills.string = kills;
    this.anim.play('kill-pop');
  },
  hide: function hide() {
    this.node.active = false;
  } // called every frame, uncomment this function to activate update callback
  // update: function (dt) {
  // },

});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcUmVuZGVyXFxLaWxsRGlzcGxheS5qcyJdLCJuYW1lcyI6WyJjYyIsIkNsYXNzIiwiQ29tcG9uZW50IiwicHJvcGVydGllcyIsImxhYmVsS2lsbHMiLCJMYWJlbCIsImFuaW0iLCJBbmltYXRpb24iLCJwbGF5S2lsbCIsImtpbGxzIiwibm9kZSIsImFjdGl2ZSIsInN0cmluZyIsInBsYXkiLCJoaWRlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBQSxFQUFFLENBQUNDLEtBQUgsQ0FBUztBQUNMLGFBQVNELEVBQUUsQ0FBQ0UsU0FEUDtBQUdMQyxFQUFBQSxVQUFVLEVBQUU7QUFDUkMsSUFBQUEsVUFBVSxFQUFFSixFQUFFLENBQUNLLEtBRFA7QUFFUkMsSUFBQUEsSUFBSSxFQUFFTixFQUFFLENBQUNPO0FBRkQsR0FIUDtBQVFMQyxFQUFBQSxRQVJLLG9CQVFLQyxLQVJMLEVBUVk7QUFDYixTQUFLQyxJQUFMLENBQVVDLE1BQVYsR0FBbUIsSUFBbkI7QUFDQSxTQUFLUCxVQUFMLENBQWdCUSxNQUFoQixHQUF5QkgsS0FBekI7QUFDQSxTQUFLSCxJQUFMLENBQVVPLElBQVYsQ0FBZSxVQUFmO0FBQ0gsR0FaSTtBQWNMQyxFQUFBQSxJQWRLLGtCQWNHO0FBQ0osU0FBS0osSUFBTCxDQUFVQyxNQUFWLEdBQW1CLEtBQW5CO0FBQ0gsR0FoQkksQ0FrQkw7QUFDQTtBQUVBOztBQXJCSyxDQUFUIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJjYy5DbGFzcyh7XG4gICAgZXh0ZW5kczogY2MuQ29tcG9uZW50LFxuXG4gICAgcHJvcGVydGllczoge1xuICAgICAgICBsYWJlbEtpbGxzOiBjYy5MYWJlbCxcbiAgICAgICAgYW5pbTogY2MuQW5pbWF0aW9uXG4gICAgfSxcbiAgICBcbiAgICBwbGF5S2lsbCAoa2lsbHMpIHtcbiAgICAgICAgdGhpcy5ub2RlLmFjdGl2ZSA9IHRydWU7XG4gICAgICAgIHRoaXMubGFiZWxLaWxscy5zdHJpbmcgPSBraWxscztcbiAgICAgICAgdGhpcy5hbmltLnBsYXkoJ2tpbGwtcG9wJyk7XG4gICAgfSxcbiAgICBcbiAgICBoaWRlICgpIHtcbiAgICAgICAgdGhpcy5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgIH1cblxuICAgIC8vIGNhbGxlZCBldmVyeSBmcmFtZSwgdW5jb21tZW50IHRoaXMgZnVuY3Rpb24gdG8gYWN0aXZhdGUgdXBkYXRlIGNhbGxiYWNrXG4gICAgLy8gdXBkYXRlOiBmdW5jdGlvbiAoZHQpIHtcblxuICAgIC8vIH0sXG59KTtcbiJdfQ==