
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/Actors/Spawn.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'ff6d6lonApC6or/lKfhLG/z', 'Spawn');
// scripts/Actors/Spawn.js

"use strict";

var FoeType = require('Types').FoeType;

var Spawn = cc.Class({
  name: 'Spawn',
  properties: {
    foeType: {
      "default": FoeType.Foe0,
      type: FoeType
    },
    total: 0,
    spawnInterval: 0,
    isCompany: false
  },
  ctor: function ctor() {
    this.spawned = 0;
    this.finished = false;
  },
  spawn: function spawn(poolMng) {
    if (this.spawned >= this.total) {
      return;
    }

    var newFoe = poolMng.requestFoe(this.foeType);

    if (newFoe) {
      this.spawned++;

      if (this.spawned === this.total) {
        this.finished = true;
      }

      return newFoe;
    } else {
      cc.log('max foe count reached, will delay spawn');
      return null;
    }
  }
});
module.exports = Spawn;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcQWN0b3JzXFxTcGF3bi5qcyJdLCJuYW1lcyI6WyJGb2VUeXBlIiwicmVxdWlyZSIsIlNwYXduIiwiY2MiLCJDbGFzcyIsIm5hbWUiLCJwcm9wZXJ0aWVzIiwiZm9lVHlwZSIsIkZvZTAiLCJ0eXBlIiwidG90YWwiLCJzcGF3bkludGVydmFsIiwiaXNDb21wYW55IiwiY3RvciIsInNwYXduZWQiLCJmaW5pc2hlZCIsInNwYXduIiwicG9vbE1uZyIsIm5ld0ZvZSIsInJlcXVlc3RGb2UiLCJsb2ciLCJtb2R1bGUiLCJleHBvcnRzIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLElBQU1BLE9BQU8sR0FBR0MsT0FBTyxDQUFDLE9BQUQsQ0FBUCxDQUFpQkQsT0FBakM7O0FBRUEsSUFBTUUsS0FBSyxHQUFHQyxFQUFFLENBQUNDLEtBQUgsQ0FBUztBQUNuQkMsRUFBQUEsSUFBSSxFQUFFLE9BRGE7QUFFbkJDLEVBQUFBLFVBQVUsRUFBRTtBQUNSQyxJQUFBQSxPQUFPLEVBQUU7QUFDTCxpQkFBU1AsT0FBTyxDQUFDUSxJQURaO0FBRUxDLE1BQUFBLElBQUksRUFBRVQ7QUFGRCxLQUREO0FBS1JVLElBQUFBLEtBQUssRUFBRSxDQUxDO0FBTVJDLElBQUFBLGFBQWEsRUFBRSxDQU5QO0FBT1JDLElBQUFBLFNBQVMsRUFBRTtBQVBILEdBRk87QUFXbkJDLEVBQUFBLElBWG1CLGtCQVdYO0FBQ0osU0FBS0MsT0FBTCxHQUFlLENBQWY7QUFDQSxTQUFLQyxRQUFMLEdBQWdCLEtBQWhCO0FBQ0gsR0Fka0I7QUFlbkJDLEVBQUFBLEtBZm1CLGlCQWVaQyxPQWZZLEVBZUg7QUFDWixRQUFJLEtBQUtILE9BQUwsSUFBZ0IsS0FBS0osS0FBekIsRUFBZ0M7QUFDNUI7QUFDSDs7QUFDRCxRQUFJUSxNQUFNLEdBQUdELE9BQU8sQ0FBQ0UsVUFBUixDQUFtQixLQUFLWixPQUF4QixDQUFiOztBQUNBLFFBQUlXLE1BQUosRUFBWTtBQUNSLFdBQUtKLE9BQUw7O0FBQ0EsVUFBSSxLQUFLQSxPQUFMLEtBQWlCLEtBQUtKLEtBQTFCLEVBQWlDO0FBQzdCLGFBQUtLLFFBQUwsR0FBZ0IsSUFBaEI7QUFDSDs7QUFDRCxhQUFPRyxNQUFQO0FBQ0gsS0FORCxNQU1PO0FBQ0hmLE1BQUFBLEVBQUUsQ0FBQ2lCLEdBQUgsQ0FBTyx5Q0FBUDtBQUNBLGFBQU8sSUFBUDtBQUNIO0FBQ0o7QUE5QmtCLENBQVQsQ0FBZDtBQWlDQUMsTUFBTSxDQUFDQyxPQUFQLEdBQWlCcEIsS0FBakIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImNvbnN0IEZvZVR5cGUgPSByZXF1aXJlKCdUeXBlcycpLkZvZVR5cGU7XG5cbmNvbnN0IFNwYXduID0gY2MuQ2xhc3Moe1xuICAgIG5hbWU6ICdTcGF3bicsXG4gICAgcHJvcGVydGllczoge1xuICAgICAgICBmb2VUeXBlOiB7XG4gICAgICAgICAgICBkZWZhdWx0OiBGb2VUeXBlLkZvZTAsXG4gICAgICAgICAgICB0eXBlOiBGb2VUeXBlXG4gICAgICAgIH0sXG4gICAgICAgIHRvdGFsOiAwLFxuICAgICAgICBzcGF3bkludGVydmFsOiAwLFxuICAgICAgICBpc0NvbXBhbnk6IGZhbHNlXG4gICAgfSxcbiAgICBjdG9yICgpIHtcbiAgICAgICAgdGhpcy5zcGF3bmVkID0gMDtcbiAgICAgICAgdGhpcy5maW5pc2hlZCA9IGZhbHNlO1xuICAgIH0sXG4gICAgc3Bhd24gKHBvb2xNbmcpIHtcbiAgICAgICAgaWYgKHRoaXMuc3Bhd25lZCA+PSB0aGlzLnRvdGFsKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgbGV0IG5ld0ZvZSA9IHBvb2xNbmcucmVxdWVzdEZvZSh0aGlzLmZvZVR5cGUpO1xuICAgICAgICBpZiAobmV3Rm9lKSB7XG4gICAgICAgICAgICB0aGlzLnNwYXduZWQrKztcbiAgICAgICAgICAgIGlmICh0aGlzLnNwYXduZWQgPT09IHRoaXMudG90YWwpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmZpbmlzaGVkID0gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBuZXdGb2U7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBjYy5sb2coJ21heCBmb2UgY291bnQgcmVhY2hlZCwgd2lsbCBkZWxheSBzcGF3bicpO1xuICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgIH1cbiAgICB9XG59KTtcblxubW9kdWxlLmV4cG9ydHMgPSBTcGF3bjsiXX0=