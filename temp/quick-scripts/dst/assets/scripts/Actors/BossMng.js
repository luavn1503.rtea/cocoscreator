
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/Actors/BossMng.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'af14cbY2OJFKb7mH92ncVxo', 'BossMng');
// scripts/Actors/BossMng.js

"use strict";

var BossType = require('Types').BossType;

var Spawn = require('Spawn');

cc.Class({
  "extends": cc.Component,
  properties: {
    demonSpawn: Spawn
  },
  init: function init(game) {
    this.game = game;
    this.waveMng = game.waveMng;
    this.bossIdx = 0;
  },
  startBoss: function startBoss() {
    if (this.bossIdx === BossType.Demon) {
      this.waveMng.startBossSpawn(this.demonSpawn);
    }
  },
  endBoss: function endBoss() {
    this.bossIdx++;
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcQWN0b3JzXFxCb3NzTW5nLmpzIl0sIm5hbWVzIjpbIkJvc3NUeXBlIiwicmVxdWlyZSIsIlNwYXduIiwiY2MiLCJDbGFzcyIsIkNvbXBvbmVudCIsInByb3BlcnRpZXMiLCJkZW1vblNwYXduIiwiaW5pdCIsImdhbWUiLCJ3YXZlTW5nIiwiYm9zc0lkeCIsInN0YXJ0Qm9zcyIsIkRlbW9uIiwic3RhcnRCb3NzU3Bhd24iLCJlbmRCb3NzIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLElBQU1BLFFBQVEsR0FBR0MsT0FBTyxDQUFDLE9BQUQsQ0FBUCxDQUFpQkQsUUFBbEM7O0FBQ0EsSUFBTUUsS0FBSyxHQUFHRCxPQUFPLENBQUMsT0FBRCxDQUFyQjs7QUFFQUUsRUFBRSxDQUFDQyxLQUFILENBQVM7QUFDTCxhQUFTRCxFQUFFLENBQUNFLFNBRFA7QUFFTEMsRUFBQUEsVUFBVSxFQUFFO0FBQ1JDLElBQUFBLFVBQVUsRUFBRUw7QUFESixHQUZQO0FBTUxNLEVBQUFBLElBTkssZ0JBTUNDLElBTkQsRUFNTztBQUNSLFNBQUtBLElBQUwsR0FBWUEsSUFBWjtBQUNBLFNBQUtDLE9BQUwsR0FBZUQsSUFBSSxDQUFDQyxPQUFwQjtBQUNBLFNBQUtDLE9BQUwsR0FBZSxDQUFmO0FBQ0gsR0FWSTtBQVlMQyxFQUFBQSxTQVpLLHVCQVlRO0FBQ1QsUUFBSSxLQUFLRCxPQUFMLEtBQWlCWCxRQUFRLENBQUNhLEtBQTlCLEVBQXFDO0FBQ2pDLFdBQUtILE9BQUwsQ0FBYUksY0FBYixDQUE0QixLQUFLUCxVQUFqQztBQUNIO0FBQ0osR0FoQkk7QUFrQkxRLEVBQUFBLE9BbEJLLHFCQWtCTTtBQUNQLFNBQUtKLE9BQUw7QUFDSDtBQXBCSSxDQUFUIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCBCb3NzVHlwZSA9IHJlcXVpcmUoJ1R5cGVzJykuQm9zc1R5cGU7XG5jb25zdCBTcGF3biA9IHJlcXVpcmUoJ1NwYXduJyk7XG5cbmNjLkNsYXNzKHtcbiAgICBleHRlbmRzOiBjYy5Db21wb25lbnQsXG4gICAgcHJvcGVydGllczoge1xuICAgICAgICBkZW1vblNwYXduOiBTcGF3bixcbiAgICB9LFxuXG4gICAgaW5pdCAoZ2FtZSkge1xuICAgICAgICB0aGlzLmdhbWUgPSBnYW1lO1xuICAgICAgICB0aGlzLndhdmVNbmcgPSBnYW1lLndhdmVNbmc7XG4gICAgICAgIHRoaXMuYm9zc0lkeCA9IDA7XG4gICAgfSxcblxuICAgIHN0YXJ0Qm9zcyAoKSB7XG4gICAgICAgIGlmICh0aGlzLmJvc3NJZHggPT09IEJvc3NUeXBlLkRlbW9uKSB7XG4gICAgICAgICAgICB0aGlzLndhdmVNbmcuc3RhcnRCb3NzU3Bhd24odGhpcy5kZW1vblNwYXduKTtcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBlbmRCb3NzICgpIHtcbiAgICAgICAgdGhpcy5ib3NzSWR4Kys7XG4gICAgfVxuXG4gICAgXG5cbn0pOyJdfQ==