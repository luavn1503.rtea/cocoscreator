
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/Actors/Projectile.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'bd907OrsYVGJpMAP56xffaF', 'Projectile');
// scripts/Actors/Projectile.js

"use strict";

var ProjectileType = require('Types').ProjectileType;

cc.Class({
  "extends": cc.Component,
  properties: {
    projectileType: {
      "default": ProjectileType.Arrow,
      type: ProjectileType
    },
    sprite: cc.Sprite,
    fxBroken: cc.Animation,
    moveSpeed: 0,
    canBreak: true
  },
  // use this for initialization
  init: function init(waveMng, dir) {
    this.waveMng = waveMng;
    this.player = waveMng.player;
    var rad = Math.atan2(dir.y, dir.x);
    var deg = cc.misc.radiansToDegrees(rad);
    var rotation = 90 - deg;
    this.sprite.node.rotation = rotation;
    this.sprite.enabled = true;
    this.direction = dir.normalize();
    this.isMoving = true;
  },
  broke: function broke() {
    this.isMoving = false;
    this.sprite.enabled = false;
    this.fxBroken.node.active = true;
    this.fxBroken.play('arrow-break');
  },
  hit: function hit() {
    this.isMoving = false;
    this.onBrokenFXFinished();
  },
  onBrokenFXFinished: function onBrokenFXFinished() {
    this.fxBroken.node.active = false;
    this.waveMng.despawnProjectile(this);
  },
  update: function update(dt) {
    if (this.isMoving === false) {
      return;
    }

    var dist = this.player.node.position.sub(this.node.position).mag();

    if (dist < this.player.hurtRadius && this.player.isAlive) {
      if (this.canBreak && this.player.isAttacking) {
        this.broke();
        return;
      } else {
        this.player.dead();
        this.hit();
        return;
      }
    }

    if (this.isMoving) {
      this.node.x += this.moveSpeed * this.direction.x * dt;
      this.node.y += this.moveSpeed * this.direction.y * dt;

      if (Math.abs(this.node.x) > this.waveMng.foeGroup.width / 2 || Math.abs(this.node.y) > this.waveMng.foeGroup.height / 2) {
        this.onBrokenFXFinished();
      }
    }
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcQWN0b3JzXFxQcm9qZWN0aWxlLmpzIl0sIm5hbWVzIjpbIlByb2plY3RpbGVUeXBlIiwicmVxdWlyZSIsImNjIiwiQ2xhc3MiLCJDb21wb25lbnQiLCJwcm9wZXJ0aWVzIiwicHJvamVjdGlsZVR5cGUiLCJBcnJvdyIsInR5cGUiLCJzcHJpdGUiLCJTcHJpdGUiLCJmeEJyb2tlbiIsIkFuaW1hdGlvbiIsIm1vdmVTcGVlZCIsImNhbkJyZWFrIiwiaW5pdCIsIndhdmVNbmciLCJkaXIiLCJwbGF5ZXIiLCJyYWQiLCJNYXRoIiwiYXRhbjIiLCJ5IiwieCIsImRlZyIsIm1pc2MiLCJyYWRpYW5zVG9EZWdyZWVzIiwicm90YXRpb24iLCJub2RlIiwiZW5hYmxlZCIsImRpcmVjdGlvbiIsIm5vcm1hbGl6ZSIsImlzTW92aW5nIiwiYnJva2UiLCJhY3RpdmUiLCJwbGF5IiwiaGl0Iiwib25Ccm9rZW5GWEZpbmlzaGVkIiwiZGVzcGF3blByb2plY3RpbGUiLCJ1cGRhdGUiLCJkdCIsImRpc3QiLCJwb3NpdGlvbiIsInN1YiIsIm1hZyIsImh1cnRSYWRpdXMiLCJpc0FsaXZlIiwiaXNBdHRhY2tpbmciLCJkZWFkIiwiYWJzIiwiZm9lR3JvdXAiLCJ3aWR0aCIsImhlaWdodCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxJQUFNQSxjQUFjLEdBQUdDLE9BQU8sQ0FBQyxPQUFELENBQVAsQ0FBaUJELGNBQXhDOztBQUVBRSxFQUFFLENBQUNDLEtBQUgsQ0FBUztBQUNMLGFBQVNELEVBQUUsQ0FBQ0UsU0FEUDtBQUdMQyxFQUFBQSxVQUFVLEVBQUU7QUFDUkMsSUFBQUEsY0FBYyxFQUFFO0FBQ1osaUJBQVNOLGNBQWMsQ0FBQ08sS0FEWjtBQUVaQyxNQUFBQSxJQUFJLEVBQUVSO0FBRk0sS0FEUjtBQUtSUyxJQUFBQSxNQUFNLEVBQUVQLEVBQUUsQ0FBQ1EsTUFMSDtBQU1SQyxJQUFBQSxRQUFRLEVBQUVULEVBQUUsQ0FBQ1UsU0FOTDtBQU9SQyxJQUFBQSxTQUFTLEVBQUUsQ0FQSDtBQVFSQyxJQUFBQSxRQUFRLEVBQUU7QUFSRixHQUhQO0FBY0w7QUFDQUMsRUFBQUEsSUFmSyxnQkFlQ0MsT0FmRCxFQWVVQyxHQWZWLEVBZWU7QUFDaEIsU0FBS0QsT0FBTCxHQUFlQSxPQUFmO0FBQ0EsU0FBS0UsTUFBTCxHQUFjRixPQUFPLENBQUNFLE1BQXRCO0FBQ0EsUUFBSUMsR0FBRyxHQUFHQyxJQUFJLENBQUNDLEtBQUwsQ0FBV0osR0FBRyxDQUFDSyxDQUFmLEVBQWtCTCxHQUFHLENBQUNNLENBQXRCLENBQVY7QUFDQSxRQUFJQyxHQUFHLEdBQUd0QixFQUFFLENBQUN1QixJQUFILENBQVFDLGdCQUFSLENBQXlCUCxHQUF6QixDQUFWO0FBQ0EsUUFBSVEsUUFBUSxHQUFHLEtBQUdILEdBQWxCO0FBQ0EsU0FBS2YsTUFBTCxDQUFZbUIsSUFBWixDQUFpQkQsUUFBakIsR0FBNEJBLFFBQTVCO0FBQ0EsU0FBS2xCLE1BQUwsQ0FBWW9CLE9BQVosR0FBc0IsSUFBdEI7QUFDQSxTQUFLQyxTQUFMLEdBQWlCYixHQUFHLENBQUNjLFNBQUosRUFBakI7QUFDQSxTQUFLQyxRQUFMLEdBQWdCLElBQWhCO0FBQ0gsR0F6Qkk7QUEyQkxDLEVBQUFBLEtBM0JLLG1CQTJCSTtBQUNMLFNBQUtELFFBQUwsR0FBZ0IsS0FBaEI7QUFDQSxTQUFLdkIsTUFBTCxDQUFZb0IsT0FBWixHQUFzQixLQUF0QjtBQUNBLFNBQUtsQixRQUFMLENBQWNpQixJQUFkLENBQW1CTSxNQUFuQixHQUE0QixJQUE1QjtBQUNBLFNBQUt2QixRQUFMLENBQWN3QixJQUFkLENBQW1CLGFBQW5CO0FBQ0gsR0FoQ0k7QUFrQ0xDLEVBQUFBLEdBbENLLGlCQWtDRTtBQUNILFNBQUtKLFFBQUwsR0FBZ0IsS0FBaEI7QUFDQSxTQUFLSyxrQkFBTDtBQUNILEdBckNJO0FBdUNMQSxFQUFBQSxrQkF2Q0ssZ0NBdUNpQjtBQUNsQixTQUFLMUIsUUFBTCxDQUFjaUIsSUFBZCxDQUFtQk0sTUFBbkIsR0FBNEIsS0FBNUI7QUFDQSxTQUFLbEIsT0FBTCxDQUFhc0IsaUJBQWIsQ0FBK0IsSUFBL0I7QUFDSCxHQTFDSTtBQTRDTEMsRUFBQUEsTUE1Q0ssa0JBNENHQyxFQTVDSCxFQTRDTztBQUNSLFFBQUksS0FBS1IsUUFBTCxLQUFrQixLQUF0QixFQUE2QjtBQUN6QjtBQUNIOztBQUVELFFBQUlTLElBQUksR0FBRyxLQUFLdkIsTUFBTCxDQUFZVSxJQUFaLENBQWlCYyxRQUFqQixDQUEwQkMsR0FBMUIsQ0FBOEIsS0FBS2YsSUFBTCxDQUFVYyxRQUF4QyxFQUFrREUsR0FBbEQsRUFBWDs7QUFDQSxRQUFJSCxJQUFJLEdBQUcsS0FBS3ZCLE1BQUwsQ0FBWTJCLFVBQW5CLElBQWlDLEtBQUszQixNQUFMLENBQVk0QixPQUFqRCxFQUEwRDtBQUN0RCxVQUFJLEtBQUtoQyxRQUFMLElBQWlCLEtBQUtJLE1BQUwsQ0FBWTZCLFdBQWpDLEVBQThDO0FBQzFDLGFBQUtkLEtBQUw7QUFDQTtBQUNILE9BSEQsTUFHTztBQUNILGFBQUtmLE1BQUwsQ0FBWThCLElBQVo7QUFDQSxhQUFLWixHQUFMO0FBQ0E7QUFDSDtBQUNKOztBQUVELFFBQUksS0FBS0osUUFBVCxFQUFtQjtBQUNmLFdBQUtKLElBQUwsQ0FBVUwsQ0FBVixJQUFlLEtBQUtWLFNBQUwsR0FBaUIsS0FBS2lCLFNBQUwsQ0FBZVAsQ0FBaEMsR0FBb0NpQixFQUFuRDtBQUNBLFdBQUtaLElBQUwsQ0FBVU4sQ0FBVixJQUFlLEtBQUtULFNBQUwsR0FBaUIsS0FBS2lCLFNBQUwsQ0FBZVIsQ0FBaEMsR0FBb0NrQixFQUFuRDs7QUFDQSxVQUFJcEIsSUFBSSxDQUFDNkIsR0FBTCxDQUFTLEtBQUtyQixJQUFMLENBQVVMLENBQW5CLElBQXdCLEtBQUtQLE9BQUwsQ0FBYWtDLFFBQWIsQ0FBc0JDLEtBQXRCLEdBQTRCLENBQXBELElBQ0EvQixJQUFJLENBQUM2QixHQUFMLENBQVMsS0FBS3JCLElBQUwsQ0FBVU4sQ0FBbkIsSUFBd0IsS0FBS04sT0FBTCxDQUFha0MsUUFBYixDQUFzQkUsTUFBdEIsR0FBNkIsQ0FEekQsRUFDNEQ7QUFDeEQsYUFBS2Ysa0JBQUw7QUFDSDtBQUNKO0FBQ0o7QUFyRUksQ0FBVCIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgUHJvamVjdGlsZVR5cGUgPSByZXF1aXJlKCdUeXBlcycpLlByb2plY3RpbGVUeXBlO1xuXG5jYy5DbGFzcyh7XG4gICAgZXh0ZW5kczogY2MuQ29tcG9uZW50LFxuXG4gICAgcHJvcGVydGllczoge1xuICAgICAgICBwcm9qZWN0aWxlVHlwZToge1xuICAgICAgICAgICAgZGVmYXVsdDogUHJvamVjdGlsZVR5cGUuQXJyb3csXG4gICAgICAgICAgICB0eXBlOiBQcm9qZWN0aWxlVHlwZVxuICAgICAgICB9LFxuICAgICAgICBzcHJpdGU6IGNjLlNwcml0ZSxcbiAgICAgICAgZnhCcm9rZW46IGNjLkFuaW1hdGlvbixcbiAgICAgICAgbW92ZVNwZWVkOiAwLFxuICAgICAgICBjYW5CcmVhazogdHJ1ZVxuICAgIH0sXG5cbiAgICAvLyB1c2UgdGhpcyBmb3IgaW5pdGlhbGl6YXRpb25cbiAgICBpbml0ICh3YXZlTW5nLCBkaXIpIHtcbiAgICAgICAgdGhpcy53YXZlTW5nID0gd2F2ZU1uZztcbiAgICAgICAgdGhpcy5wbGF5ZXIgPSB3YXZlTW5nLnBsYXllcjtcbiAgICAgICAgbGV0IHJhZCA9IE1hdGguYXRhbjIoZGlyLnksIGRpci54KTtcbiAgICAgICAgbGV0IGRlZyA9IGNjLm1pc2MucmFkaWFuc1RvRGVncmVlcyhyYWQpO1xuICAgICAgICBsZXQgcm90YXRpb24gPSA5MC1kZWc7XG4gICAgICAgIHRoaXMuc3ByaXRlLm5vZGUucm90YXRpb24gPSByb3RhdGlvbjtcbiAgICAgICAgdGhpcy5zcHJpdGUuZW5hYmxlZCA9IHRydWU7XG4gICAgICAgIHRoaXMuZGlyZWN0aW9uID0gZGlyLm5vcm1hbGl6ZSgpO1xuICAgICAgICB0aGlzLmlzTW92aW5nID0gdHJ1ZTtcbiAgICB9LFxuXG4gICAgYnJva2UgKCkge1xuICAgICAgICB0aGlzLmlzTW92aW5nID0gZmFsc2U7XG4gICAgICAgIHRoaXMuc3ByaXRlLmVuYWJsZWQgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5meEJyb2tlbi5ub2RlLmFjdGl2ZSA9IHRydWU7XG4gICAgICAgIHRoaXMuZnhCcm9rZW4ucGxheSgnYXJyb3ctYnJlYWsnKTtcbiAgICB9LFxuXG4gICAgaGl0ICgpIHtcbiAgICAgICAgdGhpcy5pc01vdmluZyA9IGZhbHNlO1xuICAgICAgICB0aGlzLm9uQnJva2VuRlhGaW5pc2hlZCgpO1xuICAgIH0sXG5cbiAgICBvbkJyb2tlbkZYRmluaXNoZWQgKCkge1xuICAgICAgICB0aGlzLmZ4QnJva2VuLm5vZGUuYWN0aXZlID0gZmFsc2U7XG4gICAgICAgIHRoaXMud2F2ZU1uZy5kZXNwYXduUHJvamVjdGlsZSh0aGlzKTtcbiAgICB9LFxuXG4gICAgdXBkYXRlIChkdCkge1xuICAgICAgICBpZiAodGhpcy5pc01vdmluZyA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIGxldCBkaXN0ID0gdGhpcy5wbGF5ZXIubm9kZS5wb3NpdGlvbi5zdWIodGhpcy5ub2RlLnBvc2l0aW9uKS5tYWcoKTtcbiAgICAgICAgaWYgKGRpc3QgPCB0aGlzLnBsYXllci5odXJ0UmFkaXVzICYmIHRoaXMucGxheWVyLmlzQWxpdmUpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLmNhbkJyZWFrICYmIHRoaXMucGxheWVyLmlzQXR0YWNraW5nKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5icm9rZSgpO1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy5wbGF5ZXIuZGVhZCgpO1xuICAgICAgICAgICAgICAgIHRoaXMuaGl0KCk7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHRoaXMuaXNNb3ZpbmcpIHtcbiAgICAgICAgICAgIHRoaXMubm9kZS54ICs9IHRoaXMubW92ZVNwZWVkICogdGhpcy5kaXJlY3Rpb24ueCAqIGR0O1xuICAgICAgICAgICAgdGhpcy5ub2RlLnkgKz0gdGhpcy5tb3ZlU3BlZWQgKiB0aGlzLmRpcmVjdGlvbi55ICogZHQ7XG4gICAgICAgICAgICBpZiAoTWF0aC5hYnModGhpcy5ub2RlLngpID4gdGhpcy53YXZlTW5nLmZvZUdyb3VwLndpZHRoLzIgIHx8XG4gICAgICAgICAgICAgICAgTWF0aC5hYnModGhpcy5ub2RlLnkpID4gdGhpcy53YXZlTW5nLmZvZUdyb3VwLmhlaWdodC8yKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5vbkJyb2tlbkZYRmluaXNoZWQoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn0pO1xuIl19