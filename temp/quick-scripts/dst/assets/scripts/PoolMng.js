
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/PoolMng.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '15ae69NDTlDzprf2vQCDLLb', 'PoolMng');
// scripts/PoolMng.js

"use strict";

var NodePool = require('NodePool');

var FoeType = require('Types').FoeType;

var ProjectileType = require('Types').ProjectileType;

cc.Class({
  "extends": cc.Component,
  properties: {
    foePools: {
      "default": [],
      type: NodePool
    },
    projectilePools: {
      "default": [],
      type: NodePool
    }
  },
  // use this for initialization
  init: function init() {
    for (var i = 0; i < this.foePools.length; ++i) {
      this.foePools[i].init();
    }

    for (var _i = 0; _i < this.projectilePools.length; ++_i) {
      this.projectilePools[_i].init();
    }
  },
  requestFoe: function requestFoe(foeType) {
    var thePool = this.foePools[foeType];

    if (thePool.idx >= 0) {
      return thePool.request();
    } else {
      return null;
    }
  },
  returnFoe: function returnFoe(foeType, obj) {
    var thePool = this.foePools[foeType];

    if (thePool.idx < thePool.size) {
      thePool["return"](obj);
    } else {
      cc.log('Return obj to a full pool, something has gone wrong');
      return;
    }
  },
  requestProjectile: function requestProjectile(type) {
    var thePool = this.projectilePools[type];

    if (thePool.idx >= 0) {
      return thePool.request();
    } else {
      return null;
    }
  },
  returnProjectile: function returnProjectile(type, obj) {
    var thePool = this.projectilePools[type];

    if (thePool.idx < thePool.size) {
      thePool["return"](obj);
    } else {
      cc.log('Return obj to a full pool, something has gone wrong');
      return;
    }
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcUG9vbE1uZy5qcyJdLCJuYW1lcyI6WyJOb2RlUG9vbCIsInJlcXVpcmUiLCJGb2VUeXBlIiwiUHJvamVjdGlsZVR5cGUiLCJjYyIsIkNsYXNzIiwiQ29tcG9uZW50IiwicHJvcGVydGllcyIsImZvZVBvb2xzIiwidHlwZSIsInByb2plY3RpbGVQb29scyIsImluaXQiLCJpIiwibGVuZ3RoIiwicmVxdWVzdEZvZSIsImZvZVR5cGUiLCJ0aGVQb29sIiwiaWR4IiwicmVxdWVzdCIsInJldHVybkZvZSIsIm9iaiIsInNpemUiLCJsb2ciLCJyZXF1ZXN0UHJvamVjdGlsZSIsInJldHVyblByb2plY3RpbGUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsSUFBTUEsUUFBUSxHQUFHQyxPQUFPLENBQUMsVUFBRCxDQUF4Qjs7QUFDQSxJQUFNQyxPQUFPLEdBQUdELE9BQU8sQ0FBQyxPQUFELENBQVAsQ0FBaUJDLE9BQWpDOztBQUNBLElBQU1DLGNBQWMsR0FBR0YsT0FBTyxDQUFDLE9BQUQsQ0FBUCxDQUFpQkUsY0FBeEM7O0FBRUFDLEVBQUUsQ0FBQ0MsS0FBSCxDQUFTO0FBQ0wsYUFBU0QsRUFBRSxDQUFDRSxTQURQO0FBR0xDLEVBQUFBLFVBQVUsRUFBRTtBQUNSQyxJQUFBQSxRQUFRLEVBQUU7QUFDTixpQkFBUyxFQURIO0FBRU5DLE1BQUFBLElBQUksRUFBRVQ7QUFGQSxLQURGO0FBS1JVLElBQUFBLGVBQWUsRUFBRTtBQUNiLGlCQUFTLEVBREk7QUFFYkQsTUFBQUEsSUFBSSxFQUFFVDtBQUZPO0FBTFQsR0FIUDtBQWNMO0FBQ0FXLEVBQUFBLElBZkssa0JBZUc7QUFDSixTQUFLLElBQUlDLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUcsS0FBS0osUUFBTCxDQUFjSyxNQUFsQyxFQUEwQyxFQUFFRCxDQUE1QyxFQUErQztBQUMzQyxXQUFLSixRQUFMLENBQWNJLENBQWQsRUFBaUJELElBQWpCO0FBQ0g7O0FBRUQsU0FBSyxJQUFJQyxFQUFDLEdBQUcsQ0FBYixFQUFnQkEsRUFBQyxHQUFHLEtBQUtGLGVBQUwsQ0FBcUJHLE1BQXpDLEVBQWlELEVBQUVELEVBQW5ELEVBQXNEO0FBQ2xELFdBQUtGLGVBQUwsQ0FBcUJFLEVBQXJCLEVBQXdCRCxJQUF4QjtBQUNIO0FBQ0osR0F2Qkk7QUF5QkxHLEVBQUFBLFVBekJLLHNCQXlCT0MsT0F6QlAsRUF5QmdCO0FBQ2pCLFFBQUlDLE9BQU8sR0FBRyxLQUFLUixRQUFMLENBQWNPLE9BQWQsQ0FBZDs7QUFDQSxRQUFJQyxPQUFPLENBQUNDLEdBQVIsSUFBZSxDQUFuQixFQUFzQjtBQUNsQixhQUFPRCxPQUFPLENBQUNFLE9BQVIsRUFBUDtBQUNILEtBRkQsTUFFTztBQUNILGFBQU8sSUFBUDtBQUNIO0FBQ0osR0FoQ0k7QUFrQ0xDLEVBQUFBLFNBbENLLHFCQWtDTUosT0FsQ04sRUFrQ2VLLEdBbENmLEVBa0NvQjtBQUNyQixRQUFJSixPQUFPLEdBQUcsS0FBS1IsUUFBTCxDQUFjTyxPQUFkLENBQWQ7O0FBQ0EsUUFBSUMsT0FBTyxDQUFDQyxHQUFSLEdBQWNELE9BQU8sQ0FBQ0ssSUFBMUIsRUFBZ0M7QUFDNUJMLE1BQUFBLE9BQU8sVUFBUCxDQUFlSSxHQUFmO0FBQ0gsS0FGRCxNQUVPO0FBQ0hoQixNQUFBQSxFQUFFLENBQUNrQixHQUFILENBQU8scURBQVA7QUFDQTtBQUNIO0FBQ0osR0ExQ0k7QUE0Q0xDLEVBQUFBLGlCQTVDSyw2QkE0Q2NkLElBNUNkLEVBNENvQjtBQUNyQixRQUFJTyxPQUFPLEdBQUcsS0FBS04sZUFBTCxDQUFxQkQsSUFBckIsQ0FBZDs7QUFDQSxRQUFJTyxPQUFPLENBQUNDLEdBQVIsSUFBZSxDQUFuQixFQUFzQjtBQUNsQixhQUFPRCxPQUFPLENBQUNFLE9BQVIsRUFBUDtBQUNILEtBRkQsTUFFTztBQUNILGFBQU8sSUFBUDtBQUNIO0FBQ0osR0FuREk7QUFxRExNLEVBQUFBLGdCQXJESyw0QkFxRGFmLElBckRiLEVBcURtQlcsR0FyRG5CLEVBcUR3QjtBQUN6QixRQUFJSixPQUFPLEdBQUcsS0FBS04sZUFBTCxDQUFxQkQsSUFBckIsQ0FBZDs7QUFDQSxRQUFJTyxPQUFPLENBQUNDLEdBQVIsR0FBY0QsT0FBTyxDQUFDSyxJQUExQixFQUFnQztBQUM1QkwsTUFBQUEsT0FBTyxVQUFQLENBQWVJLEdBQWY7QUFDSCxLQUZELE1BRU87QUFDSGhCLE1BQUFBLEVBQUUsQ0FBQ2tCLEdBQUgsQ0FBTyxxREFBUDtBQUNBO0FBQ0g7QUFDSjtBQTdESSxDQUFUIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCBOb2RlUG9vbCA9IHJlcXVpcmUoJ05vZGVQb29sJyk7XG5jb25zdCBGb2VUeXBlID0gcmVxdWlyZSgnVHlwZXMnKS5Gb2VUeXBlO1xuY29uc3QgUHJvamVjdGlsZVR5cGUgPSByZXF1aXJlKCdUeXBlcycpLlByb2plY3RpbGVUeXBlO1xuXG5jYy5DbGFzcyh7XG4gICAgZXh0ZW5kczogY2MuQ29tcG9uZW50LFxuXG4gICAgcHJvcGVydGllczoge1xuICAgICAgICBmb2VQb29sczoge1xuICAgICAgICAgICAgZGVmYXVsdDogW10sXG4gICAgICAgICAgICB0eXBlOiBOb2RlUG9vbFxuICAgICAgICB9LFxuICAgICAgICBwcm9qZWN0aWxlUG9vbHM6IHtcbiAgICAgICAgICAgIGRlZmF1bHQ6IFtdLFxuICAgICAgICAgICAgdHlwZTogTm9kZVBvb2xcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICAvLyB1c2UgdGhpcyBmb3IgaW5pdGlhbGl6YXRpb25cbiAgICBpbml0ICgpIHtcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLmZvZVBvb2xzLmxlbmd0aDsgKytpKSB7XG4gICAgICAgICAgICB0aGlzLmZvZVBvb2xzW2ldLmluaXQoKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5wcm9qZWN0aWxlUG9vbHMubGVuZ3RoOyArK2kpIHtcbiAgICAgICAgICAgIHRoaXMucHJvamVjdGlsZVBvb2xzW2ldLmluaXQoKTtcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICByZXF1ZXN0Rm9lIChmb2VUeXBlKSB7XG4gICAgICAgIGxldCB0aGVQb29sID0gdGhpcy5mb2VQb29sc1tmb2VUeXBlXTtcbiAgICAgICAgaWYgKHRoZVBvb2wuaWR4ID49IDApIHtcbiAgICAgICAgICAgIHJldHVybiB0aGVQb29sLnJlcXVlc3QoKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICB9XG4gICAgfSxcblxuICAgIHJldHVybkZvZSAoZm9lVHlwZSwgb2JqKSB7XG4gICAgICAgIGxldCB0aGVQb29sID0gdGhpcy5mb2VQb29sc1tmb2VUeXBlXTtcbiAgICAgICAgaWYgKHRoZVBvb2wuaWR4IDwgdGhlUG9vbC5zaXplKSB7XG4gICAgICAgICAgICB0aGVQb29sLnJldHVybihvYmopO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY2MubG9nKCdSZXR1cm4gb2JqIHRvIGEgZnVsbCBwb29sLCBzb21ldGhpbmcgaGFzIGdvbmUgd3JvbmcnKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICByZXF1ZXN0UHJvamVjdGlsZSAodHlwZSkge1xuICAgICAgICBsZXQgdGhlUG9vbCA9IHRoaXMucHJvamVjdGlsZVBvb2xzW3R5cGVdO1xuICAgICAgICBpZiAodGhlUG9vbC5pZHggPj0gMCkge1xuICAgICAgICAgICAgcmV0dXJuIHRoZVBvb2wucmVxdWVzdCgpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgcmV0dXJuUHJvamVjdGlsZSAodHlwZSwgb2JqKSB7XG4gICAgICAgIGxldCB0aGVQb29sID0gdGhpcy5wcm9qZWN0aWxlUG9vbHNbdHlwZV07XG4gICAgICAgIGlmICh0aGVQb29sLmlkeCA8IHRoZVBvb2wuc2l6ZSkge1xuICAgICAgICAgICAgdGhlUG9vbC5yZXR1cm4ob2JqKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGNjLmxvZygnUmV0dXJuIG9iaiB0byBhIGZ1bGwgcG9vbCwgc29tZXRoaW5nIGhhcyBnb25lIHdyb25nJyk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICB9XG59KTtcbiJdfQ==