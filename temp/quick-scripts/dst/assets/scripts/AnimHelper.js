
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/AnimHelper.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'd6ff4kVMSdOh7u6WwhErR6E', 'AnimHelper');
// scripts/AnimHelper.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {
    particleToPlay: cc.ParticleSystem,
    finishHandler: cc.Component.EventHandler,
    fireHandler: cc.Component.EventHandler
  },
  // use this for initialization
  playParticle: function playParticle() {
    if (this.particleToPlay) {
      this.particleToPlay.resetSystem();
    }
  },
  fire: function fire() {
    cc.Component.EventHandler.emitEvents([this.fireHandler]);
  },
  finish: function finish() {
    cc.Component.EventHandler.emitEvents([this.finishHandler]);
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcQW5pbUhlbHBlci5qcyJdLCJuYW1lcyI6WyJjYyIsIkNsYXNzIiwiQ29tcG9uZW50IiwicHJvcGVydGllcyIsInBhcnRpY2xlVG9QbGF5IiwiUGFydGljbGVTeXN0ZW0iLCJmaW5pc2hIYW5kbGVyIiwiRXZlbnRIYW5kbGVyIiwiZmlyZUhhbmRsZXIiLCJwbGF5UGFydGljbGUiLCJyZXNldFN5c3RlbSIsImZpcmUiLCJlbWl0RXZlbnRzIiwiZmluaXNoIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBQSxFQUFFLENBQUNDLEtBQUgsQ0FBUztBQUNMLGFBQVNELEVBQUUsQ0FBQ0UsU0FEUDtBQUdMQyxFQUFBQSxVQUFVLEVBQUU7QUFDUkMsSUFBQUEsY0FBYyxFQUFFSixFQUFFLENBQUNLLGNBRFg7QUFFUkMsSUFBQUEsYUFBYSxFQUFFTixFQUFFLENBQUNFLFNBQUgsQ0FBYUssWUFGcEI7QUFHUkMsSUFBQUEsV0FBVyxFQUFFUixFQUFFLENBQUNFLFNBQUgsQ0FBYUs7QUFIbEIsR0FIUDtBQVNMO0FBQ0FFLEVBQUFBLFlBVkssMEJBVVc7QUFDWixRQUFJLEtBQUtMLGNBQVQsRUFBeUI7QUFDckIsV0FBS0EsY0FBTCxDQUFvQk0sV0FBcEI7QUFDSDtBQUNKLEdBZEk7QUFnQkxDLEVBQUFBLElBaEJLLGtCQWdCRztBQUNKWCxJQUFBQSxFQUFFLENBQUNFLFNBQUgsQ0FBYUssWUFBYixDQUEwQkssVUFBMUIsQ0FBcUMsQ0FBQyxLQUFLSixXQUFOLENBQXJDO0FBQ0gsR0FsQkk7QUFvQkxLLEVBQUFBLE1BcEJLLG9CQW9CSztBQUNOYixJQUFBQSxFQUFFLENBQUNFLFNBQUgsQ0FBYUssWUFBYixDQUEwQkssVUFBMUIsQ0FBcUMsQ0FBQyxLQUFLTixhQUFOLENBQXJDO0FBQ0g7QUF0QkksQ0FBVCIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiY2MuQ2xhc3Moe1xuICAgIGV4dGVuZHM6IGNjLkNvbXBvbmVudCxcblxuICAgIHByb3BlcnRpZXM6IHtcbiAgICAgICAgcGFydGljbGVUb1BsYXk6IGNjLlBhcnRpY2xlU3lzdGVtLFxuICAgICAgICBmaW5pc2hIYW5kbGVyOiBjYy5Db21wb25lbnQuRXZlbnRIYW5kbGVyLFxuICAgICAgICBmaXJlSGFuZGxlcjogY2MuQ29tcG9uZW50LkV2ZW50SGFuZGxlclxuICAgIH0sXG5cbiAgICAvLyB1c2UgdGhpcyBmb3IgaW5pdGlhbGl6YXRpb25cbiAgICBwbGF5UGFydGljbGUgKCkge1xuICAgICAgICBpZiAodGhpcy5wYXJ0aWNsZVRvUGxheSkge1xuICAgICAgICAgICAgdGhpcy5wYXJ0aWNsZVRvUGxheS5yZXNldFN5c3RlbSgpO1xuICAgICAgICB9XG4gICAgfSxcbiAgICBcbiAgICBmaXJlICgpIHtcbiAgICAgICAgY2MuQ29tcG9uZW50LkV2ZW50SGFuZGxlci5lbWl0RXZlbnRzKFt0aGlzLmZpcmVIYW5kbGVyXSk7ICAgICAgICBcbiAgICB9LFxuXG4gICAgZmluaXNoICgpIHtcbiAgICAgICAgY2MuQ29tcG9uZW50LkV2ZW50SGFuZGxlci5lbWl0RXZlbnRzKFt0aGlzLmZpbmlzaEhhbmRsZXJdKTtcbiAgICB9XG5cbn0pO1xuIl19