
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/Types.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '5693aA1l/JEiq6SPSIEPrEp', 'Types');
// scripts/Types.js

"use strict";

var BossType = cc.Enum({
  Demon: -1,
  SkeletonKing: -1
});
var FoeType = cc.Enum({
  Foe0: -1,
  Foe1: -1,
  Foe2: -1,
  Foe3: -1,
  Foe5: -1,
  Foe6: -1,
  Boss1: -1,
  Boss2: -1
});
var ProjectileType = cc.Enum({
  Arrow: -1,
  Fireball: -1,
  None: 999
});
module.exports = {
  BossType: BossType,
  FoeType: FoeType,
  ProjectileType: ProjectileType
};

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcVHlwZXMuanMiXSwibmFtZXMiOlsiQm9zc1R5cGUiLCJjYyIsIkVudW0iLCJEZW1vbiIsIlNrZWxldG9uS2luZyIsIkZvZVR5cGUiLCJGb2UwIiwiRm9lMSIsIkZvZTIiLCJGb2UzIiwiRm9lNSIsIkZvZTYiLCJCb3NzMSIsIkJvc3MyIiwiUHJvamVjdGlsZVR5cGUiLCJBcnJvdyIsIkZpcmViYWxsIiwiTm9uZSIsIm1vZHVsZSIsImV4cG9ydHMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsSUFBTUEsUUFBUSxHQUFHQyxFQUFFLENBQUNDLElBQUgsQ0FBUTtBQUNyQkMsRUFBQUEsS0FBSyxFQUFFLENBQUMsQ0FEYTtBQUVyQkMsRUFBQUEsWUFBWSxFQUFFLENBQUM7QUFGTSxDQUFSLENBQWpCO0FBS0EsSUFBTUMsT0FBTyxHQUFHSixFQUFFLENBQUNDLElBQUgsQ0FBUTtBQUNwQkksRUFBQUEsSUFBSSxFQUFFLENBQUMsQ0FEYTtBQUVwQkMsRUFBQUEsSUFBSSxFQUFFLENBQUMsQ0FGYTtBQUdwQkMsRUFBQUEsSUFBSSxFQUFFLENBQUMsQ0FIYTtBQUlwQkMsRUFBQUEsSUFBSSxFQUFFLENBQUMsQ0FKYTtBQUtwQkMsRUFBQUEsSUFBSSxFQUFFLENBQUMsQ0FMYTtBQU1wQkMsRUFBQUEsSUFBSSxFQUFFLENBQUMsQ0FOYTtBQU9wQkMsRUFBQUEsS0FBSyxFQUFFLENBQUMsQ0FQWTtBQVFwQkMsRUFBQUEsS0FBSyxFQUFFLENBQUM7QUFSWSxDQUFSLENBQWhCO0FBV0EsSUFBTUMsY0FBYyxHQUFHYixFQUFFLENBQUNDLElBQUgsQ0FBUTtBQUMzQmEsRUFBQUEsS0FBSyxFQUFFLENBQUMsQ0FEbUI7QUFFM0JDLEVBQUFBLFFBQVEsRUFBRSxDQUFDLENBRmdCO0FBRzNCQyxFQUFBQSxJQUFJLEVBQUU7QUFIcUIsQ0FBUixDQUF2QjtBQU1BQyxNQUFNLENBQUNDLE9BQVAsR0FBaUI7QUFDYm5CLEVBQUFBLFFBQVEsRUFBUkEsUUFEYTtBQUViSyxFQUFBQSxPQUFPLEVBQVBBLE9BRmE7QUFHYlMsRUFBQUEsY0FBYyxFQUFkQTtBQUhhLENBQWpCIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCBCb3NzVHlwZSA9IGNjLkVudW0oe1xuICAgIERlbW9uOiAtMSxcbiAgICBTa2VsZXRvbktpbmc6IC0xXG59KTtcblxuY29uc3QgRm9lVHlwZSA9IGNjLkVudW0oe1xuICAgIEZvZTA6IC0xLFxuICAgIEZvZTE6IC0xLFxuICAgIEZvZTI6IC0xLFxuICAgIEZvZTM6IC0xLFxuICAgIEZvZTU6IC0xLFxuICAgIEZvZTY6IC0xLFxuICAgIEJvc3MxOiAtMSxcbiAgICBCb3NzMjogLTFcbn0pO1xuXG5jb25zdCBQcm9qZWN0aWxlVHlwZSA9IGNjLkVudW0oe1xuICAgIEFycm93OiAtMSxcbiAgICBGaXJlYmFsbDogLTEsXG4gICAgTm9uZTogOTk5XG59KTtcblxubW9kdWxlLmV4cG9ydHMgPSB7XG4gICAgQm9zc1R5cGUsXG4gICAgRm9lVHlwZSxcbiAgICBQcm9qZWN0aWxlVHlwZVxufTsiXX0=