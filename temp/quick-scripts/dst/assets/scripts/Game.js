
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/Game.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'f8d83fBpLZCEqij5BvvBhRZ', 'Game');
// scripts/Game.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {
    player: cc.Node,
    inGameUI: cc.Node,
    playerFX: cc.Node,
    waveMng: cc.Node,
    bossMng: cc.Node,
    poolMng: cc.Node,
    foeGroup: cc.Node,
    deathUI: cc.Node,
    gameOverUI: cc.Node,
    cameraRoot: cc.Animation
  },
  onLoad: function onLoad() {
    this.playerFX = this.playerFX.getComponent('PlayerFX');
    this.playerFX.init(this);
    this.player = this.player.getComponent('Player');
    this.player.init(this);
    this.player.node.active = false;
    this.poolMng = this.poolMng.getComponent('PoolMng');
    this.poolMng.init();
    this.waveMng = this.waveMng.getComponent('WaveMng');
    this.waveMng.init(this);
    this.bossMng = this.bossMng.getComponent('BossMng');
    this.bossMng.init(this);
    this.sortMng = this.foeGroup.getComponent('SortMng');
    this.sortMng.init(); // this.gameOverUI = this.gameOverUI.getComponent('GameOverUI');
    // this.gameOverUI.init();
    // this.gameOver = this.gameOver.getComponent('GameOver');
    // this.gameOver.init();
    // this.startWave = this.startWave.getComponent('StartWave');
    // this.
  },
  start: function start() {
    this.playerFX.playIntro();
    this.inGameUI = this.inGameUI.getComponent('InGameUI');
    this.inGameUI.init(this);
    this.deathUI = this.deathUI.getComponent('DeathUI');
    this.deathUI.init(this);
    this.gameOverUI = this.gameOverUI.getComponent('GameOverUI');
    this.gameOverUI.init(this);
  },
  pause: function pause() {
    var scheduler = cc.director.getScheduler();
    scheduler.pauseTarget(this.waveMng);
    this.sortMng.enabled = false;
  },
  resume: function resume() {
    var scheduler = cc.director.getScheduler();
    scheduler.resumeTarget(this.waveMng);
    this.sortMng.enabled = true;
  },
  cameraShake: function cameraShake() {
    this.cameraRoot.play('camera-shake');
  },
  death: function death() {
    this.deathUI.show();
    this.pause();
  },
  revive: function revive() {
    this.deathUI.hide();
    this.playerFX.playRevive();
    this.player.revive();
  },
  clearAllFoes: function clearAllFoes() {
    var nodeList = this.foeGroup.children;

    for (var i = 0; i < nodeList.length; ++i) {
      var foe = nodeList[i].getComponent('Foe');

      if (foe) {
        foe.dead();
      } else {
        var projectile = nodeList[i].getComponent('Projectile');

        if (projectile) {
          projectile.broke();
        }
      }
    }
  },
  playerReady: function playerReady() {
    this.resume();
    this.waveMng.startWave();
    this.player.node.active = true;
    this.player.ready();
  },
  gameOver: function gameOver() {
    this.deathUI.hide();
    this.gameOverUI.show();
  },
  restart: function restart() {
    cc.director.loadScene('PlayGame');
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0c1xcc2NyaXB0c1xcR2FtZS5qcyJdLCJuYW1lcyI6WyJjYyIsIkNsYXNzIiwiQ29tcG9uZW50IiwicHJvcGVydGllcyIsInBsYXllciIsIk5vZGUiLCJpbkdhbWVVSSIsInBsYXllckZYIiwid2F2ZU1uZyIsImJvc3NNbmciLCJwb29sTW5nIiwiZm9lR3JvdXAiLCJkZWF0aFVJIiwiZ2FtZU92ZXJVSSIsImNhbWVyYVJvb3QiLCJBbmltYXRpb24iLCJvbkxvYWQiLCJnZXRDb21wb25lbnQiLCJpbml0Iiwibm9kZSIsImFjdGl2ZSIsInNvcnRNbmciLCJzdGFydCIsInBsYXlJbnRybyIsInBhdXNlIiwic2NoZWR1bGVyIiwiZGlyZWN0b3IiLCJnZXRTY2hlZHVsZXIiLCJwYXVzZVRhcmdldCIsImVuYWJsZWQiLCJyZXN1bWUiLCJyZXN1bWVUYXJnZXQiLCJjYW1lcmFTaGFrZSIsInBsYXkiLCJkZWF0aCIsInNob3ciLCJyZXZpdmUiLCJoaWRlIiwicGxheVJldml2ZSIsImNsZWFyQWxsRm9lcyIsIm5vZGVMaXN0IiwiY2hpbGRyZW4iLCJpIiwibGVuZ3RoIiwiZm9lIiwiZGVhZCIsInByb2plY3RpbGUiLCJicm9rZSIsInBsYXllclJlYWR5Iiwic3RhcnRXYXZlIiwicmVhZHkiLCJnYW1lT3ZlciIsInJlc3RhcnQiLCJsb2FkU2NlbmUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUFBLEVBQUUsQ0FBQ0MsS0FBSCxDQUFTO0FBQ0wsYUFBU0QsRUFBRSxDQUFDRSxTQURQO0FBR0xDLEVBQUFBLFVBQVUsRUFBRTtBQUNSQyxJQUFBQSxNQUFNLEVBQUVKLEVBQUUsQ0FBQ0ssSUFESDtBQUVSQyxJQUFBQSxRQUFRLEVBQUVOLEVBQUUsQ0FBQ0ssSUFGTDtBQUdSRSxJQUFBQSxRQUFRLEVBQUVQLEVBQUUsQ0FBQ0ssSUFITDtBQUlSRyxJQUFBQSxPQUFPLEVBQUVSLEVBQUUsQ0FBQ0ssSUFKSjtBQUtSSSxJQUFBQSxPQUFPLEVBQUVULEVBQUUsQ0FBQ0ssSUFMSjtBQU1SSyxJQUFBQSxPQUFPLEVBQUVWLEVBQUUsQ0FBQ0ssSUFOSjtBQU9STSxJQUFBQSxRQUFRLEVBQUVYLEVBQUUsQ0FBQ0ssSUFQTDtBQVFSTyxJQUFBQSxPQUFPLEVBQUVaLEVBQUUsQ0FBQ0ssSUFSSjtBQVNSUSxJQUFBQSxVQUFVLEVBQUViLEVBQUUsQ0FBQ0ssSUFUUDtBQVVSUyxJQUFBQSxVQUFVLEVBQUVkLEVBQUUsQ0FBQ2U7QUFWUCxHQUhQO0FBZ0JMQyxFQUFBQSxNQWhCSyxvQkFnQks7QUFDTixTQUFLVCxRQUFMLEdBQWdCLEtBQUtBLFFBQUwsQ0FBY1UsWUFBZCxDQUEyQixVQUEzQixDQUFoQjtBQUNBLFNBQUtWLFFBQUwsQ0FBY1csSUFBZCxDQUFtQixJQUFuQjtBQUNBLFNBQUtkLE1BQUwsR0FBYyxLQUFLQSxNQUFMLENBQVlhLFlBQVosQ0FBeUIsUUFBekIsQ0FBZDtBQUNBLFNBQUtiLE1BQUwsQ0FBWWMsSUFBWixDQUFpQixJQUFqQjtBQUNBLFNBQUtkLE1BQUwsQ0FBWWUsSUFBWixDQUFpQkMsTUFBakIsR0FBMEIsS0FBMUI7QUFDQSxTQUFLVixPQUFMLEdBQWUsS0FBS0EsT0FBTCxDQUFhTyxZQUFiLENBQTBCLFNBQTFCLENBQWY7QUFDQSxTQUFLUCxPQUFMLENBQWFRLElBQWI7QUFDQSxTQUFLVixPQUFMLEdBQWUsS0FBS0EsT0FBTCxDQUFhUyxZQUFiLENBQTBCLFNBQTFCLENBQWY7QUFDQSxTQUFLVCxPQUFMLENBQWFVLElBQWIsQ0FBa0IsSUFBbEI7QUFDQSxTQUFLVCxPQUFMLEdBQWUsS0FBS0EsT0FBTCxDQUFhUSxZQUFiLENBQTBCLFNBQTFCLENBQWY7QUFDQSxTQUFLUixPQUFMLENBQWFTLElBQWIsQ0FBa0IsSUFBbEI7QUFDQSxTQUFLRyxPQUFMLEdBQWUsS0FBS1YsUUFBTCxDQUFjTSxZQUFkLENBQTJCLFNBQTNCLENBQWY7QUFDQSxTQUFLSSxPQUFMLENBQWFILElBQWIsR0FiTSxDQWVOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNILEdBckNJO0FBdUNMSSxFQUFBQSxLQXZDSyxtQkF1Q0k7QUFDTCxTQUFLZixRQUFMLENBQWNnQixTQUFkO0FBQ0EsU0FBS2pCLFFBQUwsR0FBZ0IsS0FBS0EsUUFBTCxDQUFjVyxZQUFkLENBQTJCLFVBQTNCLENBQWhCO0FBQ0EsU0FBS1gsUUFBTCxDQUFjWSxJQUFkLENBQW1CLElBQW5CO0FBQ0EsU0FBS04sT0FBTCxHQUFlLEtBQUtBLE9BQUwsQ0FBYUssWUFBYixDQUEwQixTQUExQixDQUFmO0FBQ0EsU0FBS0wsT0FBTCxDQUFhTSxJQUFiLENBQWtCLElBQWxCO0FBQ0EsU0FBS0wsVUFBTCxHQUFrQixLQUFLQSxVQUFMLENBQWdCSSxZQUFoQixDQUE2QixZQUE3QixDQUFsQjtBQUNBLFNBQUtKLFVBQUwsQ0FBZ0JLLElBQWhCLENBQXFCLElBQXJCO0FBQ0gsR0EvQ0k7QUFpRExNLEVBQUFBLEtBakRLLG1CQWlESTtBQUNMLFFBQUlDLFNBQVMsR0FBR3pCLEVBQUUsQ0FBQzBCLFFBQUgsQ0FBWUMsWUFBWixFQUFoQjtBQUNBRixJQUFBQSxTQUFTLENBQUNHLFdBQVYsQ0FBc0IsS0FBS3BCLE9BQTNCO0FBQ0EsU0FBS2EsT0FBTCxDQUFhUSxPQUFiLEdBQXVCLEtBQXZCO0FBQ0gsR0FyREk7QUF1RExDLEVBQUFBLE1BdkRLLG9CQXVESztBQUNOLFFBQUlMLFNBQVMsR0FBR3pCLEVBQUUsQ0FBQzBCLFFBQUgsQ0FBWUMsWUFBWixFQUFoQjtBQUNBRixJQUFBQSxTQUFTLENBQUNNLFlBQVYsQ0FBdUIsS0FBS3ZCLE9BQTVCO0FBQ0EsU0FBS2EsT0FBTCxDQUFhUSxPQUFiLEdBQXVCLElBQXZCO0FBQ0gsR0EzREk7QUE2RExHLEVBQUFBLFdBN0RLLHlCQTZEVTtBQUNYLFNBQUtsQixVQUFMLENBQWdCbUIsSUFBaEIsQ0FBcUIsY0FBckI7QUFDSCxHQS9ESTtBQWlFTEMsRUFBQUEsS0FqRUssbUJBaUVJO0FBQ0wsU0FBS3RCLE9BQUwsQ0FBYXVCLElBQWI7QUFDQSxTQUFLWCxLQUFMO0FBQ0gsR0FwRUk7QUFzRUxZLEVBQUFBLE1BdEVLLG9CQXNFSztBQUNOLFNBQUt4QixPQUFMLENBQWF5QixJQUFiO0FBQ0EsU0FBSzlCLFFBQUwsQ0FBYytCLFVBQWQ7QUFDQSxTQUFLbEMsTUFBTCxDQUFZZ0MsTUFBWjtBQUNILEdBMUVJO0FBNEVMRyxFQUFBQSxZQTVFSywwQkE0RVc7QUFDWixRQUFJQyxRQUFRLEdBQUcsS0FBSzdCLFFBQUwsQ0FBYzhCLFFBQTdCOztBQUNBLFNBQUssSUFBSUMsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR0YsUUFBUSxDQUFDRyxNQUE3QixFQUFxQyxFQUFFRCxDQUF2QyxFQUEwQztBQUN0QyxVQUFJRSxHQUFHLEdBQUdKLFFBQVEsQ0FBQ0UsQ0FBRCxDQUFSLENBQVl6QixZQUFaLENBQXlCLEtBQXpCLENBQVY7O0FBQ0EsVUFBSTJCLEdBQUosRUFBUztBQUNMQSxRQUFBQSxHQUFHLENBQUNDLElBQUo7QUFDSCxPQUZELE1BRU87QUFDSCxZQUFJQyxVQUFVLEdBQUdOLFFBQVEsQ0FBQ0UsQ0FBRCxDQUFSLENBQVl6QixZQUFaLENBQXlCLFlBQXpCLENBQWpCOztBQUNBLFlBQUk2QixVQUFKLEVBQWdCO0FBQ1pBLFVBQUFBLFVBQVUsQ0FBQ0MsS0FBWDtBQUNIO0FBQ0o7QUFDSjtBQUNKLEdBekZJO0FBMkZMQyxFQUFBQSxXQUFXLEVBQUUsdUJBQVk7QUFDckIsU0FBS2xCLE1BQUw7QUFDQSxTQUFLdEIsT0FBTCxDQUFheUMsU0FBYjtBQUNBLFNBQUs3QyxNQUFMLENBQVllLElBQVosQ0FBaUJDLE1BQWpCLEdBQTBCLElBQTFCO0FBQ0EsU0FBS2hCLE1BQUwsQ0FBWThDLEtBQVo7QUFDSCxHQWhHSTtBQWtHTEMsRUFBQUEsUUFBUSxFQUFFLG9CQUFZO0FBQ2xCLFNBQUt2QyxPQUFMLENBQWF5QixJQUFiO0FBQ0EsU0FBS3hCLFVBQUwsQ0FBZ0JzQixJQUFoQjtBQUNILEdBckdJO0FBdUdMaUIsRUFBQUEsT0FBTyxFQUFFLG1CQUFZO0FBQ2pCcEQsSUFBQUEsRUFBRSxDQUFDMEIsUUFBSCxDQUFZMkIsU0FBWixDQUFzQixVQUF0QjtBQUNIO0FBekdJLENBQVQiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbImNjLkNsYXNzKHtcbiAgICBleHRlbmRzOiBjYy5Db21wb25lbnQsXG5cbiAgICBwcm9wZXJ0aWVzOiB7XG4gICAgICAgIHBsYXllcjogY2MuTm9kZSxcbiAgICAgICAgaW5HYW1lVUk6IGNjLk5vZGUsXG4gICAgICAgIHBsYXllckZYOiBjYy5Ob2RlLFxuICAgICAgICB3YXZlTW5nOiBjYy5Ob2RlLFxuICAgICAgICBib3NzTW5nOiBjYy5Ob2RlLFxuICAgICAgICBwb29sTW5nOiBjYy5Ob2RlLFxuICAgICAgICBmb2VHcm91cDogY2MuTm9kZSxcbiAgICAgICAgZGVhdGhVSTogY2MuTm9kZSxcbiAgICAgICAgZ2FtZU92ZXJVSTogY2MuTm9kZSxcbiAgICAgICAgY2FtZXJhUm9vdDogY2MuQW5pbWF0aW9uXG4gICAgfSxcbiAgICBcbiAgICBvbkxvYWQgKCkge1xuICAgICAgICB0aGlzLnBsYXllckZYID0gdGhpcy5wbGF5ZXJGWC5nZXRDb21wb25lbnQoJ1BsYXllckZYJyk7XG4gICAgICAgIHRoaXMucGxheWVyRlguaW5pdCh0aGlzKTtcbiAgICAgICAgdGhpcy5wbGF5ZXIgPSB0aGlzLnBsYXllci5nZXRDb21wb25lbnQoJ1BsYXllcicpO1xuICAgICAgICB0aGlzLnBsYXllci5pbml0KHRoaXMpO1xuICAgICAgICB0aGlzLnBsYXllci5ub2RlLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICB0aGlzLnBvb2xNbmcgPSB0aGlzLnBvb2xNbmcuZ2V0Q29tcG9uZW50KCdQb29sTW5nJyk7XG4gICAgICAgIHRoaXMucG9vbE1uZy5pbml0KCk7XG4gICAgICAgIHRoaXMud2F2ZU1uZyA9IHRoaXMud2F2ZU1uZy5nZXRDb21wb25lbnQoJ1dhdmVNbmcnKTtcbiAgICAgICAgdGhpcy53YXZlTW5nLmluaXQodGhpcyk7XG4gICAgICAgIHRoaXMuYm9zc01uZyA9IHRoaXMuYm9zc01uZy5nZXRDb21wb25lbnQoJ0Jvc3NNbmcnKTtcbiAgICAgICAgdGhpcy5ib3NzTW5nLmluaXQodGhpcyk7XG4gICAgICAgIHRoaXMuc29ydE1uZyA9IHRoaXMuZm9lR3JvdXAuZ2V0Q29tcG9uZW50KCdTb3J0TW5nJyk7XG4gICAgICAgIHRoaXMuc29ydE1uZy5pbml0KCk7XG5cbiAgICAgICAgLy8gdGhpcy5nYW1lT3ZlclVJID0gdGhpcy5nYW1lT3ZlclVJLmdldENvbXBvbmVudCgnR2FtZU92ZXJVSScpO1xuICAgICAgICAvLyB0aGlzLmdhbWVPdmVyVUkuaW5pdCgpO1xuICAgICAgICAvLyB0aGlzLmdhbWVPdmVyID0gdGhpcy5nYW1lT3Zlci5nZXRDb21wb25lbnQoJ0dhbWVPdmVyJyk7XG4gICAgICAgIC8vIHRoaXMuZ2FtZU92ZXIuaW5pdCgpO1xuICAgICAgICAvLyB0aGlzLnN0YXJ0V2F2ZSA9IHRoaXMuc3RhcnRXYXZlLmdldENvbXBvbmVudCgnU3RhcnRXYXZlJyk7XG4gICAgICAgIC8vIHRoaXMuXG4gICAgfSxcblxuICAgIHN0YXJ0ICgpIHtcbiAgICAgICAgdGhpcy5wbGF5ZXJGWC5wbGF5SW50cm8oKTtcbiAgICAgICAgdGhpcy5pbkdhbWVVSSA9IHRoaXMuaW5HYW1lVUkuZ2V0Q29tcG9uZW50KCdJbkdhbWVVSScpO1xuICAgICAgICB0aGlzLmluR2FtZVVJLmluaXQodGhpcyk7XG4gICAgICAgIHRoaXMuZGVhdGhVSSA9IHRoaXMuZGVhdGhVSS5nZXRDb21wb25lbnQoJ0RlYXRoVUknKTtcbiAgICAgICAgdGhpcy5kZWF0aFVJLmluaXQodGhpcyk7XG4gICAgICAgIHRoaXMuZ2FtZU92ZXJVSSA9IHRoaXMuZ2FtZU92ZXJVSS5nZXRDb21wb25lbnQoJ0dhbWVPdmVyVUknKTtcbiAgICAgICAgdGhpcy5nYW1lT3ZlclVJLmluaXQodGhpcyk7XG4gICAgfSxcblxuICAgIHBhdXNlICgpIHtcbiAgICAgICAgbGV0IHNjaGVkdWxlciA9IGNjLmRpcmVjdG9yLmdldFNjaGVkdWxlcigpO1xuICAgICAgICBzY2hlZHVsZXIucGF1c2VUYXJnZXQodGhpcy53YXZlTW5nKTtcbiAgICAgICAgdGhpcy5zb3J0TW5nLmVuYWJsZWQgPSBmYWxzZTtcbiAgICB9LFxuXG4gICAgcmVzdW1lICgpIHtcbiAgICAgICAgbGV0IHNjaGVkdWxlciA9IGNjLmRpcmVjdG9yLmdldFNjaGVkdWxlcigpO1xuICAgICAgICBzY2hlZHVsZXIucmVzdW1lVGFyZ2V0KHRoaXMud2F2ZU1uZyk7XG4gICAgICAgIHRoaXMuc29ydE1uZy5lbmFibGVkID0gdHJ1ZTtcbiAgICB9LFxuICAgIFxuICAgIGNhbWVyYVNoYWtlICgpIHtcbiAgICAgICAgdGhpcy5jYW1lcmFSb290LnBsYXkoJ2NhbWVyYS1zaGFrZScpOyAgXG4gICAgfSxcblxuICAgIGRlYXRoICgpIHtcbiAgICAgICAgdGhpcy5kZWF0aFVJLnNob3coKTtcbiAgICAgICAgdGhpcy5wYXVzZSgpO1xuICAgIH0sXG5cbiAgICByZXZpdmUgKCkge1xuICAgICAgICB0aGlzLmRlYXRoVUkuaGlkZSgpO1xuICAgICAgICB0aGlzLnBsYXllckZYLnBsYXlSZXZpdmUoKTtcbiAgICAgICAgdGhpcy5wbGF5ZXIucmV2aXZlKCk7XG4gICAgfSxcbiAgICBcbiAgICBjbGVhckFsbEZvZXMgKCkge1xuICAgICAgICBsZXQgbm9kZUxpc3QgPSB0aGlzLmZvZUdyb3VwLmNoaWxkcmVuO1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IG5vZGVMaXN0Lmxlbmd0aDsgKytpKSB7XG4gICAgICAgICAgICBsZXQgZm9lID0gbm9kZUxpc3RbaV0uZ2V0Q29tcG9uZW50KCdGb2UnKTtcbiAgICAgICAgICAgIGlmIChmb2UpIHtcbiAgICAgICAgICAgICAgICBmb2UuZGVhZCgpOyAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgbGV0IHByb2plY3RpbGUgPSBub2RlTGlzdFtpXS5nZXRDb21wb25lbnQoJ1Byb2plY3RpbGUnKTtcbiAgICAgICAgICAgICAgICBpZiAocHJvamVjdGlsZSkge1xuICAgICAgICAgICAgICAgICAgICBwcm9qZWN0aWxlLmJyb2tlKCk7ICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgcGxheWVyUmVhZHk6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdGhpcy5yZXN1bWUoKTtcbiAgICAgICAgdGhpcy53YXZlTW5nLnN0YXJ0V2F2ZSgpO1xuICAgICAgICB0aGlzLnBsYXllci5ub2RlLmFjdGl2ZSA9IHRydWU7XG4gICAgICAgIHRoaXMucGxheWVyLnJlYWR5KCk7XG4gICAgfSxcblxuICAgIGdhbWVPdmVyOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHRoaXMuZGVhdGhVSS5oaWRlKCk7XG4gICAgICAgIHRoaXMuZ2FtZU92ZXJVSS5zaG93KCk7XG4gICAgfSxcblxuICAgIHJlc3RhcnQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgY2MuZGlyZWN0b3IubG9hZFNjZW5lKCdQbGF5R2FtZScpO1xuICAgIH1cblxufSk7XG4iXX0=