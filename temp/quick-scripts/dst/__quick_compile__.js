
(function () {
var scripts = [{"deps":{"./assets/migration/use_v2.1-2.2.1_cc.Toggle_event":2,"./assets/scripts/Render/PlayerFX":4,"./assets/scripts/UI/DeathUI":5,"./assets/scripts/Actors/Player":6,"./assets/scripts/Actors/Projectile":7,"./assets/scripts/NodePool":8,"./assets/scripts/Game":9,"./assets/scripts/PoolMng":3,"./assets/scripts/AnimHelper":11,"./assets/scripts/Actors/Spawn":12,"./assets/scripts/ShowMask":13,"./assets/scripts/Actors/BossMng":14,"./assets/scripts/Render/KillDisplay":15,"./assets/scripts/Types":16,"./assets/scripts/Actors/Move":17,"./assets/scripts/Render/SortMng":18,"./assets/scripts/UI/HomeUI":19,"./assets/scripts/UI/WaveUI":20,"./assets/scripts/UI/InGameUI":21,"./assets/scripts/Render/ComboDisplay":22,"./assets/scripts/UI/ButtonScaler":23,"./assets/scripts/UI/WaveProgress":24,"./assets/scripts/UI/PauseUI":25,"./assets/scripts/UI/GameOverUI":26,"./assets/migration/use_v2.1.x_cc.Action":27,"./assets/scripts/UI/BossProgress":28,"./assets/scripts/Actors/WaveMng":10,"./assets/scripts/Actors/Foe":1},"path":"preview-scripts/__qc_index__.js"},{"deps":{"Types":16,"Move":17},"path":"preview-scripts/assets/scripts/Actors/Foe.js"},{"deps":{},"path":"preview-scripts/assets/migration/use_v2.1-2.2.1_cc.Toggle_event.js"},{"deps":{"Types":16,"NodePool":8},"path":"preview-scripts/assets/scripts/PoolMng.js"},{"deps":{},"path":"preview-scripts/assets/scripts/Render/PlayerFX.js"},{"deps":{},"path":"preview-scripts/assets/scripts/UI/DeathUI.js"},{"deps":{},"path":"preview-scripts/assets/scripts/Actors/Player.js"},{"deps":{"Types":16},"path":"preview-scripts/assets/scripts/Actors/Projectile.js"},{"deps":{},"path":"preview-scripts/assets/scripts/NodePool.js"},{"deps":{},"path":"preview-scripts/assets/scripts/Game.js"},{"deps":{"Types":16,"Foe":1,"Spawn":12},"path":"preview-scripts/assets/scripts/Actors/WaveMng.js"},{"deps":{},"path":"preview-scripts/assets/scripts/AnimHelper.js"},{"deps":{"Types":16},"path":"preview-scripts/assets/scripts/Actors/Spawn.js"},{"deps":{},"path":"preview-scripts/assets/scripts/ShowMask.js"},{"deps":{"Types":16,"Spawn":12},"path":"preview-scripts/assets/scripts/Actors/BossMng.js"},{"deps":{},"path":"preview-scripts/assets/scripts/Render/KillDisplay.js"},{"deps":{},"path":"preview-scripts/assets/scripts/Types.js"},{"deps":{},"path":"preview-scripts/assets/scripts/Actors/Move.js"},{"deps":{},"path":"preview-scripts/assets/scripts/Render/SortMng.js"},{"deps":{},"path":"preview-scripts/assets/scripts/UI/HomeUI.js"},{"deps":{},"path":"preview-scripts/assets/scripts/UI/WaveUI.js"},{"deps":{},"path":"preview-scripts/assets/scripts/UI/InGameUI.js"},{"deps":{},"path":"preview-scripts/assets/scripts/Render/ComboDisplay.js"},{"deps":{},"path":"preview-scripts/assets/scripts/UI/ButtonScaler.js"},{"deps":{},"path":"preview-scripts/assets/scripts/UI/WaveProgress.js"},{"deps":{},"path":"preview-scripts/assets/scripts/UI/PauseUI.js"},{"deps":{},"path":"preview-scripts/assets/scripts/UI/GameOverUI.js"},{"deps":{},"path":"preview-scripts/assets/migration/use_v2.1.x_cc.Action.js"},{"deps":{},"path":"preview-scripts/assets/scripts/UI/BossProgress.js"}];
var entries = ["preview-scripts/__qc_index__.js"];

/**
 * Notice: This file can not use ES6 (for IE 11)
 */
var modules = {};
var name2path = {};

if (typeof global === 'undefined') {
    window.global = window;
}

function loadScript (src, cb) {
    if (typeof require !== 'undefined') {
        require(src);
        return cb();
    }

    // var timer = 'load ' + src;
    // console.time(timer);

    var scriptElement = document.createElement('script');

    function done() {
        // console.timeEnd(timer);
        // deallocation immediate whatever
        scriptElement.remove();
    }

    scriptElement.onload = function () {
        done();
        cb();
    };
    scriptElement.onerror = function () {
        done();
        var error = 'Failed to load ' + src;
        console.error(error);
        cb(new Error(error));
    };
    scriptElement.setAttribute('type','text/javascript');
    scriptElement.setAttribute('charset', 'utf-8');
    scriptElement.setAttribute('src', src);

    document.head.appendChild(scriptElement);
}

function loadScripts (srcs, cb) {
    var n = srcs.length;

    srcs.forEach(function (src) {
        loadScript(src, function () {
            n--;
            if (n === 0) {
                cb();
            }
        });
    })
}

function formatPath (path) {
    let destPath = window.__quick_compile_project__.destPath;
    if (destPath) {
        let prefix = 'preview-scripts';
        if (destPath[destPath.length - 1] === '/') {
            prefix += '/';
        }
        path = path.replace(prefix, destPath);
    }
    return path;
}

window.__quick_compile_project__ = {
    destPath: '',

    registerModule: function (path, module) {
        path = formatPath(path);
        modules[path].module = module;
    },

    registerModuleFunc: function (path, func) {
        path = formatPath(path);
        modules[path].func = func;

        var sections = path.split('/');
        var name = sections[sections.length - 1];
        name = name.replace(/\.(?:js|ts|json)$/i, '');
        name2path[name] = path;
    },

    require: function (request, path) {
        var m, requestScript;

        path = formatPath(path);
        if (path) {
            m = modules[path];
            if (!m) {
                console.warn('Can not find module for path : ' + path);
                return null;
            }
        }

        if (m) {
            requestScript = scripts[ m.deps[request] ];
        }
        
        path = '';
        if (!requestScript) {
            // search from name2path when request is a dynamic module name
            if (/^[\w- .]*$/.test(request)) {
                path = name2path[request];
            }

            if (!path) {
                if (CC_JSB) {
                    return require(request);
                }
                else {
                    console.warn('Can not find deps [' + request + '] for path : ' + path);
                    return null;
                }
            }
        }
        else {
            path = formatPath(requestScript.path);
        }

        m = modules[path];
        
        if (!m) {
            console.warn('Can not find module for path : ' + path);
            return null;
        }

        if (!m.module && m.func) {
            m.func();
        }

        if (!m.module) {
            console.warn('Can not find module.module for path : ' + path);
            return null;
        }

        return m.module.exports;
    },

    run: function () {
        entries.forEach(function (entry) {
            entry = formatPath(entry);
            var module = modules[entry];
            if (!module.module) {
                module.func();
            }
        });
    },

    load: function (cb) {
        var self = this;

        var srcs = scripts.map(function (script) {
            var path = formatPath(script.path);
            modules[path] = script;
        
            if (script.mtime) {
                path += ("?mtime=" + script.mtime);
            }
        
            return path;
        });

        loadScripts(srcs, function () {
            self.run();
            cb();
        });
    }
};

// Polyfill for IE 11
if (!('remove' in Element.prototype)) {
    Element.prototype.remove = function () {
        if (this.parentNode) {
            this.parentNode.removeChild(this);
        }
    };
}
})();
    