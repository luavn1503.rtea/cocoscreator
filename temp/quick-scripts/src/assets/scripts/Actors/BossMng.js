"use strict";
cc._RF.push(module, 'af14cbY2OJFKb7mH92ncVxo', 'BossMng');
// scripts/Actors/BossMng.js

"use strict";

var BossType = require('Types').BossType;

var Spawn = require('Spawn');

cc.Class({
  "extends": cc.Component,
  properties: {
    demonSpawn: Spawn
  },
  init: function init(game) {
    this.game = game;
    this.waveMng = game.waveMng;
    this.bossIdx = 0;
  },
  startBoss: function startBoss() {
    if (this.bossIdx === BossType.Demon) {
      this.waveMng.startBossSpawn(this.demonSpawn);
    }
  },
  endBoss: function endBoss() {
    this.bossIdx++;
  }
});

cc._RF.pop();