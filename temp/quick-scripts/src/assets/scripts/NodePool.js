"use strict";
cc._RF.push(module, '4762cqE38NHn451+NhWnQ5U', 'NodePool');
// scripts/NodePool.js

"use strict";

var NodePool = cc.Class({
  name: 'NodePool',
  properties: {
    prefab: cc.Prefab,
    size: 0
  },
  ctor: function ctor() {
    this.idx = 0;
    this.initList = [];
    this.list = [];
  },
  init: function init() {
    for (var i = 0; i < this.size; ++i) {
      var obj = cc.instantiate(this.prefab);
      this.initList[i] = obj;
      this.list[i] = obj;
    }

    this.idx = this.size - 1;
  },
  reset: function reset() {
    for (var i = 0; i < this.size; ++i) {
      var obj = this.initList[i];
      this.list[i] = obj;

      if (obj.active) {
        obj.active = false;
      }

      if (obj.parent) {
        obj.removeFromParent();
      }
    }

    this.idx = this.size - 1;
  },
  request: function request() {
    if (this.idx < 0) {
      cc.log("Error: the pool do not have enough free item.");
      return null;
    }

    var obj = this.list[this.idx];

    if (obj) {
      obj.active = true;
    }

    --this.idx;
    return obj;
  },
  "return": function _return(obj) {
    ++this.idx;
    obj.active = false;

    if (obj.parent) {
      obj.removeFromParent();
    }

    this.list[this.idx] = obj;
  }
});
module.exports = NodePool;

cc._RF.pop();