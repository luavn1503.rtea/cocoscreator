"use strict";
cc._RF.push(module, 'f8d83fBpLZCEqij5BvvBhRZ', 'Game');
// scripts/Game.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {
    player: cc.Node,
    inGameUI: cc.Node,
    playerFX: cc.Node,
    waveMng: cc.Node,
    bossMng: cc.Node,
    poolMng: cc.Node,
    foeGroup: cc.Node,
    deathUI: cc.Node,
    gameOverUI: cc.Node,
    cameraRoot: cc.Animation
  },
  onLoad: function onLoad() {
    this.playerFX = this.playerFX.getComponent('PlayerFX');
    this.playerFX.init(this);
    this.player = this.player.getComponent('Player');
    this.player.init(this);
    this.player.node.active = false;
    this.poolMng = this.poolMng.getComponent('PoolMng');
    this.poolMng.init();
    this.waveMng = this.waveMng.getComponent('WaveMng');
    this.waveMng.init(this);
    this.bossMng = this.bossMng.getComponent('BossMng');
    this.bossMng.init(this);
    this.sortMng = this.foeGroup.getComponent('SortMng');
    this.sortMng.init(); // this.gameOverUI = this.gameOverUI.getComponent('GameOverUI');
    // this.gameOverUI.init();
    // this.gameOver = this.gameOver.getComponent('GameOver');
    // this.gameOver.init();
    // this.startWave = this.startWave.getComponent('StartWave');
    // this.
  },
  start: function start() {
    this.playerFX.playIntro();
    this.inGameUI = this.inGameUI.getComponent('InGameUI');
    this.inGameUI.init(this);
    this.deathUI = this.deathUI.getComponent('DeathUI');
    this.deathUI.init(this);
    this.gameOverUI = this.gameOverUI.getComponent('GameOverUI');
    this.gameOverUI.init(this);
  },
  pause: function pause() {
    var scheduler = cc.director.getScheduler();
    scheduler.pauseTarget(this.waveMng);
    this.sortMng.enabled = false;
  },
  resume: function resume() {
    var scheduler = cc.director.getScheduler();
    scheduler.resumeTarget(this.waveMng);
    this.sortMng.enabled = true;
  },
  cameraShake: function cameraShake() {
    this.cameraRoot.play('camera-shake');
  },
  death: function death() {
    this.deathUI.show();
    this.pause();
  },
  revive: function revive() {
    this.deathUI.hide();
    this.playerFX.playRevive();
    this.player.revive();
  },
  clearAllFoes: function clearAllFoes() {
    var nodeList = this.foeGroup.children;

    for (var i = 0; i < nodeList.length; ++i) {
      var foe = nodeList[i].getComponent('Foe');

      if (foe) {
        foe.dead();
      } else {
        var projectile = nodeList[i].getComponent('Projectile');

        if (projectile) {
          projectile.broke();
        }
      }
    }
  },
  playerReady: function playerReady() {
    this.resume();
    this.waveMng.startWave();
    this.player.node.active = true;
    this.player.ready();
  },
  gameOver: function gameOver() {
    this.deathUI.hide();
    this.gameOverUI.show();
  },
  restart: function restart() {
    cc.director.loadScene('PlayGame');
  }
});

cc._RF.pop();