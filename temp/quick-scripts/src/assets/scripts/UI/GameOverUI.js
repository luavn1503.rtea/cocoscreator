"use strict";
cc._RF.push(module, 'c3ee4ElVWtB2Lzir0h5v/ow', 'GameOverUI');
// scripts/UI/GameOverUI.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {},
  init: function init(game) {
    this.game = game;
    this.hide();
  },
  // use this for initialization
  show: function show() {
    this.node.setPosition(0, 0);
  },
  hide: function hide() {
    this.node.x = 3000;
  },
  restart: function restart() {
    this.game.restart();
  },
  onLoad: function onLoad() {},
  startGame: function startGame() {
    cc.eventManager.pauseTarget(this.btnGroup, true);
    cc.director.loadScene('StartGame');
  }
});

cc._RF.pop();