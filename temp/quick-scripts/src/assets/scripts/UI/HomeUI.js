"use strict";
cc._RF.push(module, 'aa77bcy1MlA3bRL8zpupFoD', 'HomeUI');
// scripts/UI/HomeUI.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {
    menuAnim: {
      "default": null,
      type: cc.Animation
    },
    menuParticle: {
      "default": null,
      type: cc.ParticleSystem
    },
    btnGroup: {
      "default": null,
      type: cc.Node
    }
  },
  // use this for initialization
  onLoad: function onLoad() {},
  start: function start() {
    cc.eventManager.pauseTarget(this.btnGroup, true);
    this.scheduleOnce(function () {
      this.menuAnim.play();
      this.menuParticle.enabled = false;
    }.bind(this), 2);
  },
  showParticle: function showParticle() {
    this.menuParticle.enabled = true;
  },
  enableButtons: function enableButtons() {
    cc.eventManager.resumeTarget(this.btnGroup, true);
  },
  playGame: function playGame() {
    cc.eventManager.pauseTarget(this.btnGroup, true);
    cc.director.loadScene('PlayGame');
  }
});

cc._RF.pop();