"use strict";
cc._RF.push(module, 'a90aa7G1q5ANJGKlnUcA6SL', 'WaveProgress');
// scripts/UI/WaveProgress.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {
    bar: cc.ProgressBar,
    head: cc.Node,
    lerpDuration: 0
  },
  onLoad: function onLoad() {},
  init: function init(waveMng) {
    this.waveMng = waveMng;
    this.bar.progress = 0;
    this.curProgress = 0;
    this.destProgress = 0;
    this.timer = 0;
    this.isLerping = false;
  },
  updateProgress: function updateProgress(progress) {
    this.curProgress = this.bar.progress;
    this.destProgress = progress;
    this.timer = 0;
    this.isLerping = true;
  },
  update: function update(dt) {
    if (this.isLerping === false) {
      return;
    }

    this.timer += dt;

    if (this.timer >= this.lerpDuration) {
      this.timer = this.lerpDuration;
      this.isLerping = false;
    }

    this.bar.progress = cc.misc.lerp(this.curProgress, this.destProgress, this.timer / this.lerpDuration);
    var headPosX = this.bar.barSprite.node.width * this.bar.progress;
    this.head.x = headPosX;
  }
});

cc._RF.pop();