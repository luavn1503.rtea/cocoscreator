"use strict";
cc._RF.push(module, '178aaufFWBMjKEMUnBNqyHl', 'BossProgress');
// scripts/UI/BossProgress.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {
    fxParticle: cc.ParticleSystem,
    anim: cc.Animation
  },
  // use this for initialization
  init: function init(waveMng) {
    this.waveMng = waveMng;
  },
  show: function show() {
    this.node.active = true;
    this.anim.play('turn-red');
  },
  hide: function hide() {
    this.node.active = false;
  },
  showParticle: function showParticle() {
    this.fxParticle.resetSystem();
  }
});

cc._RF.pop();