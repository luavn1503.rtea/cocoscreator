"use strict";
cc._RF.push(module, 'c47b0ysWxNBGpK5/fuswtmF', 'PauseUI');
// scripts/UI/PauseUI.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {},
  init: function init(game) {
    this.game = game;
    this.hide();
  },
  Show_popup: function Show_popup() {
    this.node.active = true;
    this.node.opacity = 0;
    this.node.scale = 0.2;
    cc.tween(this.node).to(0.5, {
      scale: 1,
      opacity: 255
    }, {
      easing: "quartInOut"
    }).start();
  },
  Hide: function Hide() {
    var _this = this;

    cc.tween(this.node).to(0.5, {
      scale: 1,
      opacity: 255
    }, {
      easing: "quartInOut"
    }).call(function () {
      _this.node.active = false;
    }).start();
    this.node.active = false;
  },
  "continue": function _continue() {
    this.game["continue"]();
  },
  Quit: function (_Quit) {
    function Quit() {
      return _Quit.apply(this, arguments);
    }

    Quit.toString = function () {
      return _Quit.toString();
    };

    return Quit;
  }(function () {
    Quit.node.active = true;
  }),
  offSound: function (_offSound) {
    function offSound() {
      return _offSound.apply(this, arguments);
    }

    offSound.toString = function () {
      return _offSound.toString();
    };

    return offSound;
  }(function () {
    if (offSound == null) {
      this.node.active = true;
    } else {
      this.node.active = false;
    }
  }),
  open_Tutorial: function (_open_Tutorial) {
    function open_Tutorial() {
      return _open_Tutorial.apply(this, arguments);
    }

    open_Tutorial.toString = function () {
      return _open_Tutorial.toString();
    };

    return open_Tutorial;
  }(function () {
    if (open_Tutorial == null) {
      this.node.active = true;
    } else {
      this.node.active = false;
    }
  })
});

cc._RF.pop();