"use strict";
cc._RF.push(module, '0966f3/svtKzIRd+HwG3Kyd', 'DeathUI');
// scripts/UI/DeathUI.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {},
  init: function init(game) {
    this.game = game;
    this.hide();
  },
  show: function show() {
    this.node.setPosition(0, 0);
  },
  hide: function hide() {
    this.node.x = 3000;
  },
  revive: function revive() {
    this.game.revive();
  },
  gameover: function gameover() {
    this.game.gameOver();
  }
});

cc._RF.pop();