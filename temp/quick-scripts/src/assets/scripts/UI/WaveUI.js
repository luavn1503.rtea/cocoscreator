"use strict";
cc._RF.push(module, '389b4yNsLZD8oJXlec0Kfzr', 'WaveUI');
// scripts/UI/WaveUI.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {
    labelWave: cc.Label,
    anim: cc.Animation
  },
  // use this for initialization
  onLoad: function onLoad() {},
  show: function show(num) {
    this.labelWave.string = num;
    this.anim.play('wave-pop');
  },
  hide: function hide() {
    this.node.active = false;
  }
});

cc._RF.pop();