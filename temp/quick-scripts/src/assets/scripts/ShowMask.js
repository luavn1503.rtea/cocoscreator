"use strict";
cc._RF.push(module, '78c7dPnl5JF/p5GDHEQRYOC', 'ShowMask');
// scripts/ShowMask.js

"use strict";

// since mask is hidden in the scene (for editable),
// we use this script to show it at runtime
cc.Class({
  "extends": cc.Component,
  start: function start() {
    this.getComponent(cc.Sprite).enabled = true;
  }
});

cc._RF.pop();