"use strict";
cc._RF.push(module, '5693aA1l/JEiq6SPSIEPrEp', 'Types');
// scripts/Types.js

"use strict";

var BossType = cc.Enum({
  Demon: -1,
  SkeletonKing: -1
});
var FoeType = cc.Enum({
  Foe0: -1,
  Foe1: -1,
  Foe2: -1,
  Foe3: -1,
  Foe5: -1,
  Foe6: -1,
  Boss1: -1,
  Boss2: -1
});
var ProjectileType = cc.Enum({
  Arrow: -1,
  Fireball: -1,
  None: 999
});
module.exports = {
  BossType: BossType,
  FoeType: FoeType,
  ProjectileType: ProjectileType
};

cc._RF.pop();