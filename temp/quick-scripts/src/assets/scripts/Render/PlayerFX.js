"use strict";
cc._RF.push(module, '473ddOy0BlLraG4rFvYcoyb', 'PlayerFX');
// scripts/Render/PlayerFX.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {
    introAnim: cc.Animation,
    reviveAnim: cc.Animation
  },
  // use this for initialization
  init: function init(game) {
    this.game = game;
    this.introAnim.node.active = false;
    this.reviveAnim.node.active = false;
  },
  playIntro: function playIntro() {
    this.introAnim.node.active = true;
    this.introAnim.play('start');
  },
  playRevive: function playRevive() {
    this.reviveAnim.node.active = true;
    this.reviveAnim.node.setPosition(this.game.player.node.position);
    this.reviveAnim.play('revive');
  },
  introFinish: function introFinish() {
    this.game.playerReady();
    this.introAnim.node.active = false;
  },
  reviveFinish: function reviveFinish() {
    this.game.playerReady();
    this.reviveAnim.node.active = false;
  },
  reviveKill: function reviveKill() {
    // kill all enemies
    this.game.clearAllFoes();
  }
});

cc._RF.pop();