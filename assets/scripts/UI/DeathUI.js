cc.Class({
    extends: cc.Component,

    properties: {
        
    },

    init (game) {
        this.game = game;
        this.hide();
    },

    show () {
        this.node.setPosition(0, 0);
    },

    hide () {
        this.node.x = 3000;
    },

    revive () {
        this.game.revive();
    },

    gameover () {
        this.game.gameOver();
    },
  
});
