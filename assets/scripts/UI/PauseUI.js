
cc.Class({
    extends: cc.Component,

    properties: {
      
    },
    init (game) {
        this.game = game;
        this.hide();
    },

    Show_popup(){
        this.node.active = true;
        this.node.opacity =0;
        this.node.scale =0.2;
        cc.tween(this.node).to(0.5,{scale:1,opacity:255},{easing:"quartInOut"}).start();
    },

    Hide(){
        cc.tween(this.node).to(0.5,{scale:1,opacity:255},{easing:"quartInOut"})
        .call(()=>{this.node.active = false;})
        .start();
        this.node.active = false;
    },

    continue(){
        this.game.continue();
    },

    Quit(){
        Quit.node.active = true;
        
    },

    offSound()
    {
       if(offSound== null)
       {
           this.node.active = true;
       }
       else
       {
           this.node.active = false;
       }
        
    },
    open_Tutorial()
    {
        if(open_Tutorial== null)
        {
            this.node.active = true;
        }
        else
        {
            this.node.active = false;
        }
    }

});
